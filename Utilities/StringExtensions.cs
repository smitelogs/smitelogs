﻿namespace SmiteLogs.Utilities;

public static class StringExtensions
{
    public static string Join(this string[] strings, string separator)
    {
        return string.Join(separator, strings);
    }
}
