﻿namespace SmiteLogs.Utilities
{
    public class Time
    {
        public const string TimestampFormat = "yyyyMMddHHmmss";

        public static string GetTimestamp()
        {
            return DateTime.UtcNow.ToString(TimestampFormat);
        }
    }
}
