﻿using Microsoft.Data.Sqlite;
using SmiteLogs.Dao;

namespace SmiteLogs.Utilities;

public static class Database
{
    public static void CreateDatabase(string databasePath)
    {
        if (File.Exists(databasePath)) return;

        try
        {
            using SqliteConnection connection = new($"Data source={databasePath}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            string backupPath = Path.Combine(Directory.GetCurrentDirectory(), "Resources", Path.GetFileNameWithoutExtension(databasePath));
            command.CommandText = File.ReadAllText($"{backupPath}.sql");

            command.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }

    public static void BindParameters(this SqliteCommand command, IEnumerable<Parameter> parameters)
    {
        foreach (Parameter parameter in parameters)
        {
            command.Parameters.AddWithValue($"${parameter.Name}{parameter.Index}", parameter.Value ?? DBNull.Value);
        }
    }

    public static string ConstructCountCase(Parameter parameter, int thenValue = 1, int? elseValue = null)
    {
        string thenClause = $"THEN {thenValue} ";
        string elseClause = elseValue == null ? string.Empty : $"ELSE {elseValue} ";

        return $"CASE {parameter.Name} WHEN {parameter.Value} {thenClause}{elseClause}END";
    }

    public static string ConstructWhere(IEnumerable<Parameter> parameters)
    {
        string[] wheres = parameters.Select(p => $"{p.Name} {p.Operator} ${p.Name}{p.Index}").ToArray();
        return $"WHERE {string.Join(" AND ", wheres)}";
    }

    public static string AppendWhere(IEnumerable<Parameter> parameters)
    {
        string[] wheres = parameters.Select(p => $"{p.Name} {p.Operator} ${p.Name}{p.Index}").ToArray();
        return $" AND {string.Join(" AND ", wheres)}";
    }

    public static string ConstructBetween(Parameter lowerBound, Parameter upperBound)
    {
        return $"{lowerBound.Name} BETWEEN ${lowerBound.Name}{lowerBound.Index} AND ${upperBound.Name}{upperBound.Index}";
    }

    public static string AppendBetween(Parameter lowerBound, Parameter upperBound)
    {
        return $"AND {lowerBound.Name} BETWEEN ${lowerBound.Name}{lowerBound.Index} AND ${upperBound.Name}{upperBound.Index}";
    }

    public static bool GetBool(this SqliteDataReader reader, string field)
    {
        return bool.Parse(reader[field].ToString());
    }

    public static float GetFloat(this SqliteDataReader reader, string field)
    {
        return float.Parse(reader[field].ToString());
    }

    public static int GetInt(this SqliteDataReader reader, string field)
    {
        return (int)long.Parse(reader[field].ToString());
    }

    public static string GetString(this SqliteDataReader reader, string field)
    {
        return reader[field].ToString();
    }
}
