﻿using SmiteLogs.Models;
using SmiteLogs.Models.SmiteApi;
using System.Collections.Generic;

namespace SmiteLogs.Utilities;

public class UrlBuilder
{

    public static string BuildUrl(string basePath, params string[] paths)
    {
        return $"{basePath}/{paths.Join("/")}";
    }

    public static string BuildCreateSessionUrl(Developer developer, string endpoint, string format = "Json")
    {
        Signature signature = new(developer.Id, "createsession", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            signature.Timestamp
        );
    }

    public static string BuildTestSessionUrl(Developer developer, Session session, string endpoint, string format = "Json")
    {
        Signature signature = new(developer.Id, "testsession", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp
        );
    }

    public static string BuildGetDataUsedUrl(Developer developer, Session session, string endpoint, string format = "Json")
    {
        Signature signature = new(developer.Id, "getdataused", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp
        );
    }

    public static string BuildGetGodsUrl(Developer developer, Session session, string endpoint, string language, string format = "Json")
    {
        Signature signature = new(developer.Id, "getgods", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp,
            language
        );
    }

    public static string BuildGetItemsUrl(Developer developer, Session session, string endpoint, string language, string format = "Json")
    {
        Signature signature = new(developer.Id, "getitems", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp,
            language
        );
    }

    public static string BuildGetMatchDetailsUrl(Developer developer, Session session, string endpoint, string matchId, string format = "Json")
    {
        Signature signature = new(developer.Id, "getmatchdetails", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp,
            matchId
        );
    }

    public static string BuildGetMatchDetailsBatchUrl(Developer developer, Session session, string endpoint, List<string> matchIds, string format = "Json")
    {
        Signature signature = new(developer.Id, "getmatchdetailsbatch", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp,
            string.Join(',', matchIds)
        );
    }

    public static string BuildGetMatchIdsByQueueUrl(Developer developer, Session session, string endpoint, int queue, string date, string hour, string format = "Json")
    {
        Signature signature = new(developer.Id, "getmatchidsbyqueue", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp,
            queue.ToString(),
            date,
            hour
        );
    }

    public static string BuildGetMotdUrl(Developer developer, Session session, string endpoint, string format = "Json")
    {
        Signature signature = new(developer.Id, "getmotd", developer.AuthKey);
        return BuildUrl
        (
            endpoint,
            $"{signature.Method}{format}",
            developer.Id,
            signature.Generate(),
            session.Id,
            signature.Timestamp
        );
    }


    public static string BuildPingUrl(string endpoint, string format = "Json")
    {
        return BuildUrl(endpoint, $"ping{format}");
    }
}
