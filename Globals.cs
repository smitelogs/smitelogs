﻿using SmiteLogs.Controllers;
using SmiteLogs.Dao;
using SmiteLogs.Models;
using SmiteLogs.Models.Gods;
using SmiteLogs.Models.Items;
using SmiteLogs.Models.SmiteApi;
using SmiteLogs.Utilities;

namespace SmiteLogs;

public class Globals
{
    public static SmiteClient Client { get; protected set; }
    public static Configuration Config { get; protected set; }
    public static Dictionary<int, God> GodsById { get; protected set; } = new();
    public static Dictionary<int, Item> ItemsById { get; protected set; } = new();
    public static DateRange MatchDateRange;
    public static int ItemProgress;
    public static int WinRateProgress;

    public static readonly Dictionary<int, string> QueuesById = new()
    {
        //{ 426,      "Conquest" },
        { 435,      "Arena" },
        //{ 440,      "Duel (Ranked)" },
        //{ 445,      "Assault" },
        //{ 448,      "Joust" },
        { 450,      "Joust (Ranked)" },
        //{ 451,      "Conquest (Ranked)" },
        //{ 459,      "Seige" },
        //{ 466,      "Clash" },
        //{ 502,      "Duel Controller (Ranked)" },
        //{ 503,      "Joust Controller (Ranked)" },
        //{ 504,      "Conquest Controller (Ranked)" },
        { 10189,    "Slash" }
    };

    public static readonly Dictionary<string, DateRange> Patches = new()
    {
        { "s8",     new("01/26/2021 00:00:00 AM", "01/24/2022 11:59:59 PM") },
        { "s9",     new("01/25/2022 00:00:00 AM", "01/24/2023 11:59:59 PM") },
        { "8.10b",  new("11/02/2021 00:00:00 AM", "11/15/2021 11:59:59 PM") },
        { "8.11",   new("11/16/2021 00:00:00 AM", "11/29/2021 11:59:59 PM") },
        { "8.11b",  new("11/30/2021 00:00:00 AM", "12/13/2021 11:59:59 PM") },
        { "8.12",   new("12/14/2021 00:00:00 AM", "01/24/2022 11:59:59 PM") },
        { "9.1",    new("01/25/2022 00:00:00 AM", "02/07/2022 11:59:59 PM") },
        { "9.1b",   new("02/08/2022 00:00:00 AM", "02/21/2022 11:59:59 PM") },
        { "9.2",    new("02/22/2022 00:00:00 AM", "03/07/2022 11:59:59 PM") },
        { "9.2b",   new("03/08/2022 00:00:00 AM", "03/21/2022 11:59:59 PM") },
        { "9.3",    new("03/22/2022 00:00:00 AM", "04/04/2022 11:59:59 PM") },
        { "9.3b",   new("04/05/2022 00:00:00 AM", "04/18/2022 11:59:59 PM") },
        { "9.4",    new("04/19/2022 00:00:00 AM", "05/02/2022 11:59:59 PM") },
        { "9.4b",   new("05/03/2022 00:00:00 AM", "05/16/2022 11:59:59 PM") },
        { "9.5",    new("05/17/2022 00:00:00 AM", "05/30/2022 11:59:59 PM") },
        { "9.5b",   new("05/31/2022 00:00:00 AM", "06/13/2022 11:59:59 PM") },
        { "9.6",    new("06/14/2022 00:00:00 AM", "06/27/2022 11:59:59 PM") },
        { "9.6b",   new("06/28/2022 00:00:00 AM", "07/11/2022 11:59:59 PM") },
        { "9.7",    new("07/12/2022 00:00:00 AM", "07/25/2022 11:59:59 PM") },
        { "9.7b",   new("07/26/2022 00:00:00 AM", "08/08/2022 11:59:59 PM") },

        { "latest", new("07/12/2022 00:00:00 AM", "08/22/2022 11:59:59 PM") },
    };

    public static void InitializeLocalData()
    {
        LoadGods();
        LoadItems();
    }

    public static void LoadGods()
    {
        GodDao godDao = new(Config.Database);
        List<God> gods = godDao.GetGods();

        GodsById = new();
        foreach (God god in gods)
        {
            GodsById.Add(god.GodId, god);
        }
    }

    public static void LoadItems()
    {
        ItemDao itemDao = new(Config.Database);
        List<Item> items = itemDao.GetItems();

        ItemsById = new();
        foreach (Item item in items)
        {
            ItemsById.Add(item.ItemId, item);
        }
    }

    public static async Task SetupClientSession()
    {
        Config = ConfigurationManager.Load();
        ConfigurationManager.Save(Config);

        Client = new(Config.Developer);
        await Client.Ping();

        if(!await SmiteApiController.CheckSession(Config))
        {
            return;
        }

        SmiteApiController.GetDataUsed();
        Database.CreateDatabase(Config.Database);
    }
}
