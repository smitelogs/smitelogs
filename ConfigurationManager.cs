﻿using Newtonsoft.Json;
using SmiteLogs.Models;

namespace SmiteLogs;

public class ConfigurationManager
{
    public const string ResourcesDirectory = "Resources";
    public const string ConfigurationFile = "config.json";

    private static JsonSerializerSettings _serializerSettings = new()
    {
        Formatting = Formatting.Indented,
        NullValueHandling = NullValueHandling.Include
    };

    public static Configuration Load()
    {
        string configPath = Path.Combine(Directory.GetCurrentDirectory(), ResourcesDirectory, ConfigurationFile);

        if (!File.Exists(configPath))
        {
            Console.WriteLine($"Configuration file not found at: {configPath}");
            return null;
        }

        try
        {
            string json = File.ReadAllText(configPath);
            Configuration config = JsonConvert.DeserializeObject<Configuration>(json, _serializerSettings);
            Console.WriteLine($"Configuration loaded from: {configPath}");
            return config;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return null;
    }

    public static bool Save(Configuration config)
    {
        string configPath = Path.Combine(Directory.GetCurrentDirectory(), ResourcesDirectory, ConfigurationFile);
        if (File.Exists(configPath))
        {
            File.Delete(configPath);
        }

        try
        {
            string json = JsonConvert.SerializeObject(config, _serializerSettings);
            File.WriteAllText(configPath, json);
            Console.WriteLine($"Configuration saved to: {configPath}");
            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return false;
    }
}