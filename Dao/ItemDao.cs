﻿using Microsoft.Data.Sqlite;
using SmiteLogs.Models.Items;
using SmiteLogs.Utilities;
using System.Diagnostics;

namespace SmiteLogs.Dao;

public class ItemDao
{
    public const int TRANSACTION_SIZE = 25000;

    public string Database { get; set; }

    public ItemDao(string database)
    {
        Database = database;
    }

    public int BulkInsertItems(IEnumerable<Item> items)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        using SqliteConnection connection = new($"Data source={Database}");
        connection.Open();

        using SqliteTransaction transaction = connection.BeginTransaction();
        int insertedRows = 0;

        try
        {
            foreach (Item item in items)
            {
                item.Parse();

                ParameterSet parameters = new()
                {
                    new("ItemId", item.ItemId),
                    new("DeviceName", item.DeviceName),

                    new("AttackSpeed", item.AttackSpeed),
                    new("CooldownReduction", item.CooldownReduction),
                    new("CriticalStrikeChance", item.CriticalStrikeChance),
                    new("CrowdControlReduction", item.CrowdControlReduction),
                    new("Health", item.Health),
                    new("HP5", item.HP5),
                    new("MagicalLifesteal", item.MagicalLifesteal),
                    new("MagicalPenetration", item.MagicalPenetration),
                    new("MagicalPenetrationPercent", item.MagicalPenetrationPercent),
                    new("MagicalPower", item.MagicalPower),
                    new("MagicalProtection", item.MagicalProtection),
                    new("Mana", item.Mana),
                    new("MovementSpeed", item.MovementSpeed),
                    new("MP5", item.MP5),
                    new("PhysicalLifesteal", item.PhysicalLifesteal),
                    new("PhysicalPenetration", item.PhysicalPenetration),
                    new("PhysicalPenetrationPercent", item.PhysicalPenetrationPercent),
                    new("PhysicalPower", item.PhysicalPower),
                    new("PhysicalProtection", item.PhysicalProtection),

                    new("ActiveFlag", item.ActiveFlag),
                    new("ChildItemId", item.ChildItemId),
                    new("IconId", item.IconId),
                    new("ItemDescription", item.ItemDescription.SecondaryDescription),
                    new("ItemTier", item.ItemTier),
                    new("Price", item.Price),
                    new("RestrictedRoles", item.RestrictedRoles),
                    new("RootItemId", item.RootItemId),
                    new("ShortDesc", item.ShortDesc),
                    new("StartingItem", item.StartingItem),
                    new("Type", item.Type),
                    new("ItemIconUrl", item.ItemIconUrl),
                };

                using SqliteCommand command = connection.CreateCommand();

                string columns = string.Join(", ", parameters.Select(p => p.Name));
                string values = string.Join(", ", parameters.Select(p => $"${p.Name}{p.Index}"));

                command.CommandText = $"INSERT INTO Items ({columns}) VALUES({values})";
                command.BindParameters(parameters);
                command.ExecuteNonQuery();

                insertedRows++;
            }

            transaction.Commit();

            Console.WriteLine($"Inserted {insertedRows} Items ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
            return insertedRows;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return insertedRows;
    }

    public List<Item> GetItems()
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        List<Item> items = new();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            command.CommandText = "SELECT * FROM Items";

            using SqliteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Item item = new()
                {
                    ItemId = reader.GetInt("ItemId"),
                    DeviceName = reader.GetString("DeviceName"),

                    AttackSpeed = reader.GetFloat("AttackSpeed"),
                    CooldownReduction = reader.GetFloat("CooldownReduction"),
                    CriticalStrikeChance = reader.GetFloat("CriticalStrikeChance"),
                    CrowdControlReduction = reader.GetFloat("CrowdControlReduction"),
                    Health = reader.GetInt("Health"),
                    HP5 = reader.GetInt("HP5"),
                    MagicalLifesteal = reader.GetFloat("MagicalLifesteal"),
                    MagicalPenetration = reader.GetInt("MagicalPenetration"),
                    MagicalPenetrationPercent = reader.GetFloat("MagicalPenetrationPercent"),
                    MagicalPower = reader.GetInt("MagicalPower"),
                    MagicalProtection = reader.GetInt("MagicalProtection"),
                    Mana = reader.GetInt("Mana"),
                    MovementSpeed = reader.GetFloat("MovementSpeed"),
                    MP5 = reader.GetInt("MP5"),
                    PhysicalLifesteal = reader.GetFloat("PhysicalLifesteal"),
                    PhysicalPenetration = reader.GetInt("PhysicalPenetration"),
                    PhysicalPenetrationPercent = reader.GetFloat("PhysicalPenetrationPercent"),
                    PhysicalPower = reader.GetInt("PhysicalPower"),
                    PhysicalProtection = reader.GetInt("PhysicalProtection"),

                    ActiveFlag = reader.GetString("ActiveFlag"),
                    ChildItemId = reader.GetInt("ChildItemId"),
                    ItemDescription = new() { SecondaryDescription = reader.GetString("ItemDescription") },
                    ShortDesc = reader.GetString("ShortDesc"),
                    StartingItem = reader.GetBool("StartingItem"),
                    ItemTier = reader.GetInt("ItemTier"),
                    Price = reader.GetInt("Price"),
                    RestrictedRoles = reader.GetString("RestrictedRoles"),
                    RootItemId = reader.GetInt("RootItemId"),
                    ItemIconUrl = reader.GetString("ItemIconUrl"),
                    Type = reader.GetString("Type")
                };

                items.Add(item);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        Console.WriteLine($"Selected Items ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
        return items;
    }

    public void DeleteItems()
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            command.CommandText = "DELETE FROM Items";

            command.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
