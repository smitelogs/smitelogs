﻿
using Microsoft.Data.Sqlite;
using SmiteLogs.Models;
using SmiteLogs.Models.Items;
using SmiteLogs.Models.Matches;
using System.Diagnostics;
using System.Globalization;
using static SmiteLogs.Utilities.Database;

namespace SmiteLogs.Dao;

public class MatchPlayerDao
{
    public const int TRANSACTION_SIZE = 25000;

    public string Database { get; set; }

    public MatchPlayerDao(string database)
    {
        Database = database;
    }

    public long GetCount(DateRange dateRange, int? godID = null)
    {
        long count = 0;

        Stopwatch stopwatch = new();
        stopwatch.Start();

        using SqliteConnection connection = new($"Data source={Database}");
        connection.Open();

        try
        {
            using SqliteCommand command = connection.CreateCommand();
            command.CommandText = $"SELECT COUNT(*) FROM MatchPlayers WHERE EntryDateTime BETWEEN '{dateRange.FromSimple}' AND '{dateRange.ToSimple}'" + (godID.HasValue ? $" AND GodId = {godID}" : "");

            using SqliteDataReader reader = command.ExecuteReader();
            if(reader.Read())
            {
                count = reader.GetInt64(0);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return count;
    }

    public DateRange GetDateRange(DateRange dateRange)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        using SqliteConnection connection = new($"Data source={Database}");
        connection.Open();

        DateRange clampedRange = null;

        try
        {
            using SqliteCommand command = connection.CreateCommand();

            command.CommandText = $"SELECT MIN(EntryDatetime) as MinDate, MAX(EntryDatetime) as MaxDate FROM MatchPlayers WHERE EntryDateTime BETWEEN '{dateRange.FromSimple}' AND '{dateRange.ToSimple}'";

            using SqliteDataReader reader = command.ExecuteReader();

            if(reader.Read())
            {
                string from = reader.IsDBNull(0) ? DateTime.MinValue.ToString(DateRange.MATCH_FORMAT) : reader.GetDateTime(0).ToString(DateRange.MATCH_FORMAT);
                string to = reader.IsDBNull(1) ? DateTime.MaxValue.ToString(DateRange.MATCH_FORMAT) : reader.GetDateTime(1).ToString(DateRange.MATCH_FORMAT);
                clampedRange = new(from, to);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return clampedRange;
    }

    public int BulkInsertMatchPlayers(List<MatchPlayer> players)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        using SqliteConnection connection = new($"Data source={Database}");
        connection.Open();

        using SqliteTransaction transaction = connection.BeginTransaction();
        int insertedRows = 0;

        try
        {
            for (int i = 0; i < players.Count; i++)
            {
                MatchPlayer player = players[i];

                if (player == null)
                {
                    continue;
                }

                using SqliteCommand command = connection.CreateCommand();

                ParameterSet parameters = new()
                {
                    new("Id", $"{player.Match}-{i}"),
                    new("AccountLevel", player.AccountLevel),
                    new("ActiveId1", player.ActiveId1),
                    new("ActiveId2", player.ActiveId2),
                    new("ActiveId3", player.ActiveId3),
                    new("ActiveId4", player.ActiveId4),
                    new("ActivePlayerId", player.ActivePlayerId),
                    new("Assists", player.Assists),
                    new("Ban1Id", player.Ban1Id),
                    new("Ban2Id", player.Ban2Id),
                    new("Ban3Id", player.Ban3Id),
                    new("Ban4Id", player.Ban4Id),
                    new("Ban5Id", player.Ban5Id),
                    new("Ban6Id", player.Ban6Id),
                    new("Ban7Id", player.Ban7Id),
                    new("Ban8Id", player.Ban8Id),
                    new("Ban9Id", player.Ban9Id),
                    new("Ban10Id", player.Ban10Id),
                    new("Ban11Id", player.Ban11Id),
                    new("Ban12Id", player.Ban12Id),
                    new("CampsCleared", player.CampsCleared),
                    new("ConquestLosses", player.ConquestLosses),
                    new("ConquestPoints", player.ConquestPoints),
                    new("ConquestTier", player.ConquestTier),
                    new("ConquestWins", player.ConquestWins),
                    new("DamageBot", player.DamageBot),
                    new("DamageDoneInHand", player.DamageDoneInHand),
                    new("DamageMitigated", player.DamageMitigated),
                    new("DamagePlayer", player.DamagePlayer),
                    new("DamageTaken", player.DamageTaken),
                    new("Deaths", player.Deaths),
                    new("DistanceTraveled", player.DistanceTraveled),
                    new("DuelLosses", player.DuelLosses),
                    new("DuelPoints", player.DuelPoints),
                    new("DuelTier", player.DuelTier),
                    new("DuelWins", player.DuelWins),
                    new("EntryDatetime", DateTime.ParseExact(player.EntryDatetime, DateRange.MATCH_FORMAT, CultureInfo.InvariantCulture).ToString(DateRange.SIMPLE_FORMAT)),
                    new("FinalMatchLevel", player.FinalMatchLevel),
                    new("FirstBanSide", player.FirstBanSide),
                    new("GodId", player.GodId),
                    new("GoldEarned", player.GoldEarned),
                    new("GoldPerMinute", player.GoldPerMinute),
                    new("HasReplay", player.HasReplay),
                    new("Healing", player.Healing),
                    new("HealingBot", player.HealingBot),
                    new("HealingPlayerSelf", player.HealingPlayerSelf),
                    new("ItemId1", player.ItemId1),
                    new("ItemId2", player.ItemId2),
                    new("ItemId3", player.ItemId3),
                    new("ItemId4", player.ItemId4),
                    new("ItemId5", player.ItemId5),
                    new("ItemId6", player.ItemId6),
                    new("JoustLosses", player.JoustLosses),
                    new("JoustPoints", player.JoustPoints),
                    new("JoustTier", player.JoustTier),
                    new("JoustWins", player.JoustWins),
                    new("KillingSpree", player.KillingSpree),
                    new("KillsBot", player.KillsBot),
                    new("KillsDouble", player.KillsDouble),
                    new("KillsFireGiant", player.KillsFireGiant),
                    new("KillsFirstBlood", player.KillsFirstBlood),
                    new("KillsGoldFury", player.KillsGoldFury),
                    new("KillsPenta", player.KillsPenta),
                    new("KillsPhoenix", player.KillsPhoenix),
                    new("KillsPlayer", player.KillsPlayer),
                    new("KillsQuadra", player.KillsQuadra),
                    new("KillsSeigeJuggernaut", player.KillsSeigeJuggernaut),
                    new("KillsSingle", player.KillsSingle),
                    new("KillsTriple", player.KillsTriple),
                    new("KillsWildJuggernaut", player.KillsWildJuggernaut),
                    new("MasteryLevel", player.MasteryLevel),
                    new("Match", player.Match),
                    new("MatchDuration", player.MatchDuration),
                    new("MatchQueueId", player.MatchQueueId),
                    new("Minutes", player.Minutes),
                    new("MultiKillMax", player.MultiKillMax),
                    new("ObjectiveAssists", player.ObjectiveAssists),
                    new("PartyId", player.PartyId),
                    new("PlayerId", player.PlayerId),
                    new("RankStatConquest", player.RankStatConquest),
                    new("RankStatDuel", player.RankStatDuel),
                    new("RankStatJoust", player.RankStatJoust),
                    new("Region", player.Region),
                    new("SkinId", player.SkinId),
                    new("StructureDamage", player.StructureDamage),
                    new("Surrendered", player.Surrendered),
                    new("Team1Score", player.Team1Score),
                    new("Team2Score", player.Team2Score),
                    new("TeamId", player.TeamId),
                    new("TimeDeadSeconds", player.TimeDeadSeconds),
                    new("TimeInMatchSeconds", player.TimeInMatchSeconds),
                    new("TowersDestroyed", player.TowersDestroyed),
                    new("WardsPlaced", player.WardsPlaced),
                    new("WinStatus", player.WinStatus)
                };

                string columns = string.Join(", ", parameters.Select(p => p.Name));
                string values = string.Join(", ", parameters.Select(p => $"${p.Name}{p.Index}"));

                command.CommandText = $"INSERT INTO MatchPlayers ({columns}) VALUES({values})";
                command.BindParameters(parameters);
                command.ExecuteNonQuery();

                insertedRows++;
            }

            transaction.Commit();

            //Console.WriteLine($"Inserted {insertedRows} MatchPlayers ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
            return insertedRows;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return insertedRows;
    }

    public float GetWinRate(int godId, int? queue = null, string patch = null, Loadout loadout = null)
    {
        patch ??= Patches.Keys.First();
        Stopwatch stopwatch = new();
        stopwatch.Start();

        float winRate = 0f;

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            ParameterSet parameters = new()
            {
                new("GodId", godId)
            };

            if (queue != null)
            {
                parameters.Add(new("MatchQueueId", queue));
            }

            if (loadout != null)
            {
                if (loadout.ActiveId1 != null)
                {
                    parameters.Add(new("ActiveId1", loadout.ActiveId1));
                }

                if (loadout.ActiveId2 != null)
                {
                    parameters.Add(new("ActiveId2", loadout.ActiveId2));
                }

                if (loadout.ActiveId3 != null)
                {
                    parameters.Add(new("ActiveId3", loadout.ActiveId3));
                }

                if (loadout.ActiveId4 != null)
                {
                    parameters.Add(new("ActiveId4", loadout.ActiveId4));
                }

                if (loadout.ItemId1 != null)
                {
                    parameters.Add(new("ItemId1", loadout.ItemId1));
                }

                if (loadout.ItemId2 != null)
                {
                    parameters.Add(new("ItemId2", loadout.ItemId2));
                }

                if (loadout.ItemId3 != null)
                {
                    parameters.Add(new("ItemId3", loadout.ItemId3));
                }

                if (loadout.ItemId4 != null)
                {
                    parameters.Add(new("ItemId4", loadout.ItemId4));
                }

                if (loadout.ItemId5 != null)
                {
                    parameters.Add(new("ItemId5", loadout.ItemId5));
                }

                if (loadout.ItemId6 != null)
                {
                    parameters.Add(new("ItemId6", loadout.ItemId6));
                }
            }
            
            string betweenClause = "";

            if (patch != null)
            {
                try
                {
                    betweenClause = $" AND EntryDatetime BETWEEN '{Patches[patch].FromSimple}' AND '{Patches[patch].ToSimple}'";
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            string appendWhere = $"{AppendWhere(parameters)}{betweenClause}";
            string whereClause = $"{ConstructWhere(parameters)}{betweenClause}";

            command.CommandText = @$"SELECT ROUND( CAST((SELECT COUNT (*) FROM MatchPlayers WHERE WinStatus = 'Winner' {appendWhere}{betweenClause}) as real) / (SELECT COUNT (*) FROM MatchPlayers {whereClause}{betweenClause}), 4) * 100 as WinRate";
            command.BindParameters(parameters);

            using SqliteDataReader reader = command.ExecuteReader();

            reader.Read();
            object value = reader["WinRate"];
            winRate = value is not DBNull ? (float)(double)value : 0;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        string godString = $"for {GodsById[godId].Name}";
        string queueString = queue == null ? "" : $" in {QueuesById[queue.Value]}";
        string dateString = patch == null ? "" : $" for {patch}";
        string loadoutString = loadout == null ? "" : $" with loadout { loadout?.ToString()}";

        float progress = (++WinRateProgress * 100f) / (GodsById.Count() * QueuesById.Count());
        Console.WriteLine($"Calculated a {winRate} % win rate {godString}{queueString}{dateString}{loadoutString} ({(long)stopwatch.Elapsed.TotalMilliseconds} ms, {progress:0.00} %)");
        return winRate;
    }

    public List<WeightedItem> GetItemsByPopularity(int itemSlot, int? godId = null, int? queueId = null, string patch = null)
    {
        patch ??= Patches.Keys.First();

        Stopwatch stopwatch = new();
        stopwatch.Start();

        string itemSlotName = $"ItemId{Math.Clamp(itemSlot, 1, 6)}";

        List<WeightedItem> itemsByPopularity = new();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            ParameterSet parameters = new();

            if (godId != null)
            {
                parameters.Add(new("GodId", godId));
            }

            if (queueId != null)
            {
                parameters.Add(new("MatchQueueId", queueId));
            }

            string betweenClause = "";

            if (patch != null)
            {
                try
                {
                    betweenClause = $" AND EntryDatetime BETWEEN '{Patches[patch].FromSimple}' AND '{Patches[patch].ToSimple}'";
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            string appendWhere = $"{AppendWhere(parameters)}{betweenClause}";

            command.CommandText = $"SELECT {itemSlotName}, ChildItemId, ItemTier, DeviceName, COUNT({itemSlotName}) AS Popularity FROM MatchPlayers LEFT JOIN Items ON {itemSlotName} WHERE {itemSlotName} = Items.ItemId {appendWhere} GROUP BY {itemSlotName} ORDER BY Popularity DESC";
            command.BindParameters(parameters);

            using SqliteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                WeightedItem item = new()
                {
                    Patch = patch,
                    GodId = godId,
                    ItemId = reader.GetInt(itemSlotName),
                    ChildItemId = reader.GetInt("ChildItemId"),
                    ItemTier = reader.GetInt("ItemTier"),
                    ItemSlot = itemSlot,
                    Name = reader.GetString("DeviceName"),
                    Popularity = reader.GetInt("Popularity"),
                    QueueId = queueId
                };

                itemsByPopularity.Add(item);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        //Console.WriteLine($"Calculated the most popular {GodsById[godId.Value].Name} items for {QueuesById[queueId.Value]} in slot {itemSlot} ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
        return itemsByPopularity;
    }

    public WeightedItem GetItemPerformance(int itemId, int itemSlot, int? godId = null, int? queueId = null, string patch = null)
    {
        patch ??= Patches.Keys.First();
        Stopwatch stopwatch = new();
        stopwatch.Start();

        string itemSlotName = $"ItemId{Math.Clamp(itemSlot, 1, 6)}";

        WeightedItem weightedItem = new()
        {
            ItemId = itemId
        };

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            ParameterSet parameters = new();

            if (godId != null)
            {
                parameters.Add(new("GodId", godId));
            }

            if (queueId != null)
            {
                parameters.Add(new("MatchQueueId", queueId));
            }

            string betweenClause = "";

            if (patch != null)
            {
                try
                {
                    betweenClause = $" AND EntryDatetime BETWEEN '{Patches[patch].FromSimple}' AND '{Patches[patch].ToSimple}'";
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            string winCase = ConstructCountCase(new("WinStatus", "\"Winner\""));
            string itemCase = ConstructCountCase(new(itemSlotName, itemId));
            string compoundCase = ConstructCountCase(new($"({winCase}) + ({itemCase})", 2));
            string where = $"{ConstructWhere(parameters)}{betweenClause}";

            command.CommandText = $"SELECT COUNT({compoundCase}) as Wins, COUNT({itemCase}) as Popularity, COUNT(*) as TotalMatches FROM (SELECT * FROM MatchPlayers {where})";

            command.BindParameters(parameters);

            using SqliteDataReader reader = command.ExecuteReader();

            reader.Read();

            weightedItem = new()
            {
                Patch = patch,
                GodId = godId,
                ItemId = itemId,
                ItemSlot = itemSlot,
                Popularity = reader.GetInt("Popularity"),
                QueueId = queueId,
                TotalMatches = reader.GetInt("TotalMatches"),
                Wins = reader.GetInt("Wins")
            };

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        //Console.WriteLine($"Calculated optimal {GodsById[godId.Value].Name} items for {QueuesById[queueId.Value]} in slot {itemSlot} ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
        return weightedItem;
    }
}
