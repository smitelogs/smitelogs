﻿using Microsoft.Data.Sqlite;
using SmiteLogs.Models.Matches;
using System.Diagnostics;
using static SmiteLogs.Utilities.Database;

namespace SmiteLogs.Dao;

public class MatchDao
{
    public const int TRANSACTION_SIZE = 25000;
    public const int SELECT_SIZE = 20;

    public string Database { get; set; }

    public MatchDao(string database)
    {
        Database = database;
    }

    public int BulkInsertMatches(IEnumerable<Match> matches, int queue, string date)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        using SqliteConnection connection = new($"Data source={Database}");
        connection.Open();

        using SqliteTransaction transaction = connection.BeginTransaction();
        int insertedRows = 0;

        try
        {
            foreach (Match match in matches)
            {
                using SqliteCommand command = connection.CreateCommand();

                ParameterSet parameters = new()
                {
                    new("Id", match.Id),
                    new("Queue", queue),
                    new("Date", date),
                    new("Active", match.ActiveFlag)
                };

                string columns = string.Join(", ", parameters.Select(p => p.Name));
                string values = string.Join(", ", parameters.Select(p => $"${p.Name}{p.Index}"));

                command.CommandText = $"INSERT INTO Matches ({columns}) VALUES({values})";
                command.BindParameters(parameters);
                command.ExecuteNonQuery();

                insertedRows++;
            }

            transaction.Commit();

            Console.WriteLine($"Inserted {insertedRows} Matches ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
            return insertedRows;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return insertedRows;
    }

    public List<Match> SelectQueueMatchesByDate(int queue, string date)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        List<Match> matches = new();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            ParameterSet parameters = new()
            {
                new("Queue", queue),
                new("Date", date),
                new("Active", "y", Parameter.Operators.NotEqual)
            };

            command.CommandText = $"SELECT * FROM Matches {ConstructWhere(parameters)}";
            command.BindParameters(parameters);

            using SqliteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                matches.Add(
                    new()
                    {
                        Id = reader.GetString("Id"),
                        Queue = reader.GetInt("Queue"),
                        Date = reader.GetString("Date"),
                        ActiveFlag = reader.GetString("Active")
                    }
                );
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        Console.WriteLine($"Selected {matches.Count} Matches ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
        return matches;
    }

    public string SelectLatestMatchDate()
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        string matchDate = "N/A";

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();
            command.CommandText = "SELECT Date FROM Matches ORDER BY Date DESC LIMIT 1";

            using SqliteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                matchDate = reader.GetString("Date");
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return matchDate;
    }
}
