﻿using Microsoft.Data.Sqlite;
using SmiteLogs.Models.Gods;
using SmiteLogs.Utilities;
using System.Diagnostics;
using static SmiteLogs.Utilities.Database;

namespace SmiteLogs.Dao;

public class GodDao
{
    public const int TRANSACTION_SIZE = 25000;

    public string Database { get; set; }

    public GodDao(string database)
    {
        Database = database;
    }

    public int BulkInsertGods(IEnumerable<God> gods)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        using SqliteConnection connection = new($"Data source={Database}");
        connection.Open();

        using SqliteTransaction transaction = connection.BeginTransaction();
        int insertedRows = 0;

        try
        {
            foreach (God god in gods)
            {
                ParameterSet parameters = new()
                {
                    new("GodId", god.GodId),
                    new("Name", god.Name),
                    new("AttackSpeed", god.AttackSpeed),
                    new("AttackSpeedPerLevel", god.AttackSpeedPerLevel),
                    new("AutoBanned", god.AutoBanned),
                    new("Cons", god.Cons),
                    new("HP5PerLevel", god.HP5PerLevel),
                    new("Health", god.Health),
                    new("HealthPerFive", god.HealthPerFive),
                    new("HealthPerLevel", god.HealthPerLevel),
                    new("Lore", god.Lore),
                    new("MP5PerLevel", god.MP5PerLevel),
                    new("MagicalPower", god.MagicalPower),
                    new("MagicalPowerPerLevel", god.MagicalPowerPerLevel),
                    new("MagicalProtection", god.MagicalProtection),
                    new("MagicalProtectionPerLevel", god.MagicalProtectionPerLevel),
                    new("Mana", god.Mana),
                    new("ManaPerFive", god.ManaPerFive),
                    new("ManaPerLevel", god.ManaPerLevel),
                    new("OnFreeRotation", god.OnFreeRotation),
                    new("Pantheon", god.Pantheon),
                    new("PhysicalPower", god.PhysicalPower),
                    new("PhysicalPowerPerLevel", god.PhysicalPowerPerLevel),
                    new("PhysicalProtection", god.PhysicalProtection),
                    new("PhysicalProtectionPerLevel", god.PhysicalProtectionPerLevel),
                    new("Pros", god.Pros),
                    new("Roles", god.Roles),
                    new("Speed", god.Speed),
                    new("Title", god.Title),
                    new("Type", god.Type),
                    new("GodCardUrl", god.GodCardUrl),
                    new("GodIconUrl", god.GodIconUrl),
                    new("LatestGod", god.LatestGod),
                    new("GodAbility1Url", god.GodAbility1Url),
                    new("GodAbility2Url", god.GodAbility2Url),
                    new("GodAbility3Url", god.GodAbility3Url),
                    new("GodAbility4Url", god.GodAbility4Url),
                    new("GodAbility5Url", god.GodAbility5Url),
                    new("AbilityId1", god.AbilityId1),
                    new("AbilityId2", god.AbilityId2),
                    new("AbilityId3", god.AbilityId3),
                    new("AbilityId4", god.AbilityId4),
                    new("AbilityId5", god.AbilityId5),
                };

                using SqliteCommand command = connection.CreateCommand();

                string columns = string.Join(", ", parameters.Select(p => p.Name));
                string values = string.Join(", ", parameters.Select(p => $"${p.Name}{p.Index}"));

                command.CommandText = $"INSERT INTO Gods ({columns}) VALUES({values})";
                command.BindParameters(parameters);
                command.ExecuteNonQuery();

                insertedRows++;
            }

            transaction.Commit();

            Console.WriteLine($"Inserted {insertedRows} Gods ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
            return insertedRows;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        return insertedRows;
    }

    public List<int> GetGodIds()
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        List<int> godIds = new();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            command.CommandText = "SELECT GodId FROM Gods ASC";

            using SqliteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                godIds.Add(reader.GetInt("GodId"));
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        Console.WriteLine($"Selected GodIds ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
        return godIds;
    }

    public List<God> GetGods()
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        List<God> gods = new();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            command.CommandText = "SELECT * FROM Gods";

            using SqliteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                God god = new()
                {
                    GodId = reader.GetInt("GodId"),
                    Name = reader.GetString("Name"),
                    AttackSpeed = reader.GetFloat("AttackSpeed"),
                    AttackSpeedPerLevel = reader.GetFloat("AttackSpeedPerLevel"),
                    AutoBanned = reader.GetString("AutoBanned"),
                    Cons = reader.GetString("Cons"),
                    Health = reader.GetInt("Health"),
                    HealthPerFive = reader.GetInt("HealthPerFive"),
                    HealthPerLevel = reader.GetInt("HealthPerLevel"),
                    HP5PerLevel = reader.GetFloat("HP5PerLevel"),
                    Lore = reader.GetString("Lore"),
                    MagicalPower = reader.GetInt("MagicalPower"),
                    MagicalPowerPerLevel = reader.GetFloat("MagicalPowerPerLevel"),
                    MagicalProtection = reader.GetFloat("MagicalProtection"),
                    MagicalProtectionPerLevel = reader.GetFloat("MagicalProtectionPerLevel"),
                    Mana = reader.GetInt("Mana"),
                    ManaPerFive = reader.GetFloat("ManaPerFive"),
                    ManaPerLevel = reader.GetInt("ManaPerLevel"),
                    MP5PerLevel = reader.GetFloat("MP5PerLevel"),
                    OnFreeRotation = reader.GetString("OnFreeRotation"),
                    Pantheon = reader.GetString("Pantheon"),
                    PhysicalPower = reader.GetInt("PhysicalPower"),
                    PhysicalPowerPerLevel = reader.GetFloat("PhysicalPowerPerLevel"),
                    PhysicalProtection = reader.GetFloat("PhysicalProtection"),
                    PhysicalProtectionPerLevel = reader.GetFloat("PhysicalProtectionPerLevel"),
                    Pros = reader.GetString("Pros"),
                    Roles = reader.GetString("Roles"),
                    Speed = reader.GetInt("Speed"),
                    Title = reader.GetString("Title"),
                    Type = reader.GetString("Type"),
                    GodCardUrl = reader.GetString("GodCardUrl"),
                    GodIconUrl = reader.GetString("GodIconUrl"),
                    GodAbility1Url = reader.GetString("GodAbility1Url"),
                    GodAbility2Url = reader.GetString("GodAbility2Url"),
                    GodAbility3Url = reader.GetString("GodAbility3Url"),
                    GodAbility4Url = reader.GetString("GodAbility4Url"),
                    GodAbility5Url = reader.GetString("GodAbility5Url"),
                    AbilityId1 = reader.GetInt("AbilityId1"),
                    AbilityId2 = reader.GetInt("AbilityId2"),
                    AbilityId3 = reader.GetInt("AbilityId3"),
                    AbilityId4 = reader.GetInt("AbilityId4"),
                    AbilityId5 = reader.GetInt("AbilityId5")
                };

                gods.Add(god);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        Console.WriteLine($"Selected Gods ({(long)stopwatch.Elapsed.TotalMilliseconds} ms)");
        return gods;
    }

    public void DeleteGods()
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        try
        {
            using SqliteConnection connection = new($"Data source={Database}");
            connection.Open();

            using SqliteCommand command = connection.CreateCommand();

            command.CommandText = "DELETE FROM Gods";

            command.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
