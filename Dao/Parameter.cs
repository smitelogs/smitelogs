﻿namespace SmiteLogs.Dao;

public class Parameter
{

    public enum Operators
    {
        Equal,
        NotEqual,
        Is,
        Like,
        GreaterThan,
        LessThan,
        GreaterThanOrEqual,
        LessThanOrEqual,
        Between
    }

    public string Name { get; set; }
    public object Value { get; set; }
    public string Operator { get; set; }
    public int Index = 0;

    public Parameter(string name, object value, Operators op = Operators.Equal)
    {
        Name = name;
        Value = value?.ToString();
        Operator = OperatorAsString(op);
    }

    public static string OperatorAsString(Operators op)
    {
        return op switch
        {
            Operators.Equal => "=",
            Operators.NotEqual => "!=",
            Operators.Is => "IS",
            Operators.Like => "LIKE",
            Operators.GreaterThan => ">",
            Operators.LessThan => "<",
            Operators.GreaterThanOrEqual => ">=",
            Operators.LessThanOrEqual => "<=",
            Operators.Between => "BETWEEN",
            _ => "=",
        };
    }
}
