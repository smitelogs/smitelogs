﻿namespace SmiteLogs.Dao;

public class ParameterSet : HashSet<Parameter>
{
    public new void Add(Parameter parameter)
    {
        parameter.Index = this.Where(p => p.Name.Equals(parameter.Name)).Count();
        base.Add(parameter);
    }
}
