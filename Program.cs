﻿global using static SmiteLogs.Globals;
using SmiteLogs.Controllers;
using SmiteLogs.Dao;
using SmiteLogs.Models.Gods;
using SmiteLogs.Models.Items;

namespace SmiteLogs;

public class Program
{
    public static readonly Dictionary<string, Func<string[], Task>> Commands = new()
    {
        { "exit", Exit },
        { "get_gods", GetGods },
        { "get_items", GetItems },
        { "get_matches", GetMatches },
        { "god_details", GodDetails },
        { "gods", Gods },
        { "help", Help },
        { "item_details", ItemDetails },
        { "items", Items },
        { "last_update", LastUpdate },
        { "ping", Ping },
        { "queues", Queues },
        { "update_builds", UpdateBuilds },
        { "update_winrates", UpdateGodWinRates },
    };

    public static async Task Main(string[] args)
    {
        Console.WriteLine("Initializing SmiteLogs...");

        await SetupClientSession();
        InitializeLocalData();

        Console.WriteLine("\nEnter a command or 'help' for more information:");

        while (true)
        {
            try
            {
                Console.Write("> ");

                string[] commands = Console.ReadLine().ToLower().Trim().Split(' ');
                Console.WriteLine();

                string command = commands.FirstOrDefault();
                if (Commands.ContainsKey(command))
                {
                    Func<string[], Task> func = Commands[command];
                    await func.Invoke(commands.Skip(1).ToArray());
                }
                else
                {
                    Console.WriteLine($"'{command}' is not a recognized command. Use 'help' for more information.");
                }

                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        Console.WriteLine("Done.");
    }

    public static async Task Exit(string[] args)
    {
        Environment.Exit(0);
    }

    public static async Task GetGods(string[] args)
    {
        await SmiteApiController.GetGods(Config);
        LoadGods();
        await Gods(args);
    }

    public static async Task GetItems(string[] args)
    {
        await SmiteApiController.GetItems(Config);
        LoadItems();
        await Items(args);
    }

    public static async Task GetMatches(string[] args)
    {
        string date = null;

        if (args != null && args.Length > 0)
        {
            date = args[0];
        }

        await SmiteApiController.GetAllMatchIdsByDate(args);
        Console.WriteLine("Matches queried.");
    }

    public static async Task GodDetails(string[] args)
    {
        if (args.Length == 0)
        {
            return;
        }

        int godId = int.Parse(args[0]);

        if (!GodsById.ContainsKey(godId))
        {
            return;
        }

        God god = GodsById[godId];

        Console.WriteLine($"God Details ({god.Name}):");
        Console.Write(god.ToString());
    }

    public static async Task Gods(string[] args)
    {
        Console.WriteLine("Gods:");
        foreach (God god in GodsById.Values)
        {
            Console.WriteLine($"{god.GodId}: {god.Name}");
        }
    }

    public static async Task Help(string[] args)
    {
        Console.WriteLine("Commands:\n");
        Console.WriteLine("exit:\n  Exits the application.\n");
        Console.WriteLine("get_gods:\n  Gets all gods from the Smite API and updates the database.\n");
        Console.WriteLine("get_items:\n  Gets all items from the Smite API and updates the database.\n");
        Console.WriteLine("get_matches <yyyy-MM-dd>:\n  Gets matches from the Smite API on the given date.\n");
        Console.WriteLine("god_details <godId>:\n  Display information about the god with the provided GodID.\n");
        Console.WriteLine("gods:\n  Displays all gods by their GodID.\n");
        Console.WriteLine("help:\n  Displays available commands.\n");
        Console.WriteLine("item_details <itemId>:\n  Display information about the item with the provided ItemID.\n");
        Console.WriteLine("items:\n  Displays all items by their ItemID.\n");
        Console.WriteLine("last_update:\n  Displays the latest match date recorded in the database.\n");
        Console.WriteLine("ping:\n  Pings the Smite API.\n");
        Console.WriteLine("queues:\n  Displays the available queues.\n");
        Console.WriteLine("update_builds <patch>:\n  Exports build data from the cache.\n");
        Console.WriteLine("update_winrates <patch>:\n  Exports win rate data from the cache");
    }

    public static async Task ItemDetails(string[] args)
    {
        if (args.Length == 0) return;

        int itemId = int.Parse(args[0]);

        if (!ItemsById.ContainsKey(itemId)) return;

        Item item = ItemsById[itemId];

        Console.WriteLine($"Item Details ({item.DeviceName}):");
        Console.Write(item.ToString());
    }

    public static async Task Items(string[] args)
    {
        Console.WriteLine("Items:");
        foreach (Item item in ItemsById.Values)
        {
            Console.WriteLine($"{item.ItemId}: {item.DeviceName}");
        }
    }

    public static async Task LastUpdate(string[] args)
    {
        MatchDao matchDao = new(Config.Database);
        Console.WriteLine($"Last Updated: {matchDao.SelectLatestMatchDate()}");
    }

    public static async Task Ping(string[] args)
    {
        await Client.Ping();
    }

    public static async Task Queues(string[] args)
    {
        Console.WriteLine("Queues:");
        foreach (KeyValuePair<int, string> kvp in QueuesById)
        {
            Console.WriteLine($"{kvp.Key}: {kvp.Value}");
        }
    }

    public static async Task UpdateBuilds(string[] args)
    {
        string patch = null;
        if (args != null && args.Length > 0)
        {
            patch = args[0];
        }

        patch ??= Patches.Keys.Last();

        Console.WriteLine("This process will export build data from the local cache.");
        Console.WriteLine("Would you like to re-calculate build data? (Y/n)");
        Console.Write("> ");

        string input = Console.ReadLine().ToLower().Trim();
        bool update;

        switch (input)
        {
            case "y":
                update = true;
                break;

            case "n":
                update = false;
                break;

            default:
                Console.WriteLine($"'{input}' is not a valid input.");
                return;
        }

        ItemProgress = 0;

        List<Task> taskList = new();

        foreach (int godId in GodsById.Keys)
        {
            taskList.Add(Task.Run(() => BuildController.ExportOptimalGodBuildsAsPage(godId, patch, update)));
        }

        Task t = Task.WhenAll(taskList.ToArray());
        t.Wait();

        Console.WriteLine("Builds updated.");
    }

    public static async Task UpdateGodWinRates(string[] args)
    {
        string patch = null;
        if (args != null && args.Length > 0)
        {
            patch = args[0];
        }

        patch ??= Patches.Keys.First();

        Console.WriteLine("This process will export win rate data from the local cache.");
        Console.WriteLine("Would you like to re-calculate win rate data? (Y/n");
        Console.Write("> ");

        string input = Console.ReadLine().ToLower().Trim();
        bool update;

        switch (input)
        {
            case "y":
                update = true;
                break;

            case "n":
                update = false;
                break;

            default:
                Console.WriteLine($"'{input}' is not a valid input.");
                return;
        }

        WinRateProgress = 0;

        WinRateController.ExportGodWinRatesByQueueAsPage(patch, update);
        Console.WriteLine("Win rates updated.");
    }
}