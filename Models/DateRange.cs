﻿using System.Globalization;

namespace SmiteLogs.Models;

public class DateRange
{
    public DateTime From { get; set; }
    public DateTime To { get; set; }

    public const string MATCH_FORMAT = "M/d/yyyy h:mm:ss tt";
    public const string SIMPLE_FORMAT = "yyyy-MM-dd";
    public const string WEB_FORMAT = "MMMM dd yyyy";

    public DateRange(string from, string to)
    {
        From = DateTime.ParseExact(from, MATCH_FORMAT, CultureInfo.InvariantCulture);
        To = DateTime.ParseExact(to, MATCH_FORMAT, CultureInfo.InvariantCulture);
    }

    public string FromMatch => From.ToString(MATCH_FORMAT);
    public string ToMatch => To.ToString(MATCH_FORMAT);
    public string FromSimple => From.ToString(SIMPLE_FORMAT);
    public string ToSimple => To.ToString(SIMPLE_FORMAT);
    public string FromWeb => From.ToString(WEB_FORMAT);
    public string ToWeb => To.ToString(WEB_FORMAT);
}
