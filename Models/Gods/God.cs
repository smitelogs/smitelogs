﻿using Newtonsoft.Json;
using SmiteLogs.Models.Gods.Abilities;
using System.Text;

namespace SmiteLogs.Models.Gods;

public class God
{
    public string Ability1 { get; set; }
    public string Ability2 { get; set; }
    public string Ability3 { get; set; }
    public string Ability4 { get; set; }
    public string Ability5 { get; set; }
    public int AbilityId1 { get; set; }
    public int AbilityId2 { get; set; }
    public int AbilityId3 { get; set; }
    public int AbilityId4 { get; set; }
    public int AbilityId5 { get; set; }
    public Ability Ability_1 { get; set; }
    public Ability Ability_2 { get; set; }
    public Ability Ability_3 { get; set; }
    public Ability Ability_4 { get; set; }
    public Ability Ability_5 { get; set; }
    public float AttackSpeed { get; set; }
    public float AttackSpeedPerLevel { get; set; }
    public string AutoBanned { get; set; }
    public string Cons { get; set; }
    public int Health { get; set; }
    public float HealthPerFive { get; set; }
    public int HealthPerLevel { get; set; }
    public float HP5PerLevel { get; set; }
    public string Lore { get; set; }
    public int MagicalPower { get; set; }
    public float MagicalPowerPerLevel { get; set; }

    [JsonProperty("MagicProtection")]
    public float MagicalProtection { get; set; }

    [JsonProperty("MagicProtectionPerLevel")]
    public float MagicalProtectionPerLevel { get; set; }

    public int Mana { get; set; }
    public float ManaPerFive { get; set; }
    public int ManaPerLevel { get; set; }
    public float MP5PerLevel { get; set; }
    public string Name { get; set; }
    public string OnFreeRotation { get; set; }
    public string Pantheon { get; set; }
    public int PhysicalPower { get; set; }
    public float PhysicalPowerPerLevel { get; set; }
    public float PhysicalProtection { get; set; }
    public float PhysicalProtectionPerLevel { get; set; }
    public string Pros { get; set; }
    public string Roles { get; set; }
    public int Speed { get; set; }
    public string Title { get; set; }
    public string Type { get; set; }

    [JsonProperty("abilityDescription1")]
    public ItemDescription AbilityDescription1 { get; set; }

    [JsonProperty("abilityDescription2")]
    public ItemDescription AbilityDescription2 { get; set; }

    [JsonProperty("abilityDescription3")]
    public ItemDescription AbilityDescription3 { get; set; }

    [JsonProperty("abilityDescription4")]
    public ItemDescription AbilityDescription4 { get; set; }

    [JsonProperty("abilityDescription5")]
    public ItemDescription AbilityDescription5 { get; set; }

    [JsonProperty("basicAttack")]
    public ItemDescription BasicAttack { get; set; }

    [JsonProperty("godAbility1_URL")]
    public string GodAbility1Url { get; set; }

    [JsonProperty("godAbility2_URL")]
    public string GodAbility2Url { get; set; }

    [JsonProperty("godAbility3_URL")]
    public string GodAbility3Url { get; set; }

    [JsonProperty("godAbility4_URL")]
    public string GodAbility4Url { get; set; }

    [JsonProperty("godAbility5_URL")]
    public string GodAbility5Url { get; set; }

    [JsonProperty("godCard_URL")]
    public string GodCardUrl { get; set; }

    [JsonProperty("godIcon_URL")]
    public string GodIconUrl { get; set; }

    [JsonProperty("id")]
    public int GodId { get; set; }

    [JsonProperty("lastestGod")]
    public string LatestGod { get; set; }

    [JsonProperty("ret_msg")]
    public string ReturnMessage { get; set; }

    public new string ToString()
    {
        StringBuilder builder = new();

        builder.Append("\n-- Basic Information --\n");
        if (Cons != null) builder.Append($"Cons: {Cons}\n");
        if (Pantheon != null) builder.Append($"Pantheon: {Pantheon}\n");
        if (Pros != null) builder.Append($"Pros: {Pros}\n");
        if (Roles != null) builder.Append($"Roles: {Roles}\n");
        if (Title != null) builder.Append($"Title: {Title}\n");
        if (Type != null) builder.Append($"Type: {Type}\n");

        builder.Append("\n-- Stats --\n");
        if (AttackSpeed != 0) builder.Append($"AttackSpeed: {AttackSpeed}\n");
        if (AttackSpeedPerLevel != 0) builder.Append($"AttackSpeed: {AttackSpeedPerLevel}\n");
        if (Health != 0) builder.Append($"Health: {Health}\n");
        if (HealthPerFive != 0) builder.Append($"HealthPerFive: {HealthPerFive}\n");
        if (HealthPerLevel != 0) builder.Append($"HealthPerLevel: {HealthPerLevel}\n");
        if (HP5PerLevel != 0) builder.Append($"HP5PerLevel: {HP5PerLevel}\n");
        if (MagicalPower != 0) builder.Append($"MagicalPower: {MagicalPower}\n");
        if (MagicalPowerPerLevel != 0) builder.Append($"MagicalPowerPerLevel: {MagicalPowerPerLevel}\n");
        if (MagicalProtection != 0) builder.Append($"MagicalProtection: {MagicalProtection}\n");
        if (MagicalProtectionPerLevel != 0) builder.Append($"MagicalProtectionPerLevel: {MagicalProtectionPerLevel}\n");
        if (Mana != 0) builder.Append($"Mana: {Mana}\n");
        if (ManaPerFive != 0) builder.Append($"ManaPerFive: {ManaPerFive}\n");
        if (ManaPerLevel != 0) builder.Append($"ManaPerLevel: {ManaPerLevel}\n");
        if (MP5PerLevel != 0) builder.Append($"MP5PerLevel: {MP5PerLevel}\n");
        if (PhysicalPower != 0) builder.Append($"PhysicalPower: {PhysicalPower}\n");
        if (PhysicalPowerPerLevel != 0) builder.Append($"PhysicalPowerPerLevel: {PhysicalPowerPerLevel}\n");
        if (PhysicalProtection != 0) builder.Append($"PhysicalProtection: {PhysicalProtection}\n");
        if (PhysicalProtectionPerLevel != 0) builder.Append($"PhysicalProtectionPerLevel: {PhysicalProtectionPerLevel}\n");
        if (Speed != 0) builder.Append($"Speed: {Speed}\n");

        return builder.ToString();
    }
}
