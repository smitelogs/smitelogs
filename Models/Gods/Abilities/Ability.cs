﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Gods.Abilities;

public class Ability
{
    public AbilityDescription Description { get; set; }

    [JsonProperty("Id")]
    public int ID { get; set; }

    public string Summary { get; set; }

    [JsonProperty("URL")]
    public string Url { get; set; }

}
