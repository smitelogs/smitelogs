﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Gods.Abilities;

public class AbilityDescription
{
    [JsonProperty("itemDescription")]
    public ItemDescription ItemDescription { get; set; }

    [JsonProperty("rankItems")]
    public List<MenuItem> RankItems { get; set; }
}
