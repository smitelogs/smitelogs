﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Gods.Abilities;

public class ItemDescription
{
    [JsonProperty("cooldown")]
    public string Cooldown { get; set; }

    [JsonProperty("cost")]
    public string Cost { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("menuItems")]
    public List<MenuItem> MenuItems { get; set; }
}
