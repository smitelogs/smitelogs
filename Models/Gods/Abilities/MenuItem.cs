﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Gods.Abilities;

public class MenuItem
{
    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("value")]
    public string Value { get; set; }
}
