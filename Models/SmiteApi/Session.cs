﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.SmiteApi;

public class Session
{
    /*
    Access limits:
        concurrent_sessions:  50
        sessions_per_day: 500
        session_time_limit:  15 minutes
        request_day_limit:  7500
     */

    [JsonProperty("ret_msg")]
    public string Result { get; set; }

    [JsonProperty("session_id")]
    public string Id { get; set; }

    [JsonProperty("timestamp")]
    public string Timestamp { get; set; }

    public bool IsExpired()
    {
        DateTime creationDate = DateTime.Parse(Timestamp);
        DateTime expirationDate = creationDate + TimeSpan.FromMinutes(15);
        return DateTime.UtcNow > expirationDate;
    }
}
