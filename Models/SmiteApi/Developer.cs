﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.SmiteApi;

public class Developer
{
    [JsonProperty("devId")]
    public string Id { get; set; }

    [JsonProperty("authKey")]
    public string AuthKey { get; set; }
}