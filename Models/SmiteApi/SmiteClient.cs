﻿using Newtonsoft.Json;
using SmiteLogs.Controllers;
using SmiteLogs.Models.Gods;
using SmiteLogs.Models.Items;
using SmiteLogs.Models.Matches;
using System.Net;
using static SmiteLogs.Utilities.UrlBuilder;

namespace SmiteLogs.Models.SmiteApi;

public class SmiteClient : HttpClient
{

    public const string Endpoint = "https://api.smitegame.com/smiteapi.svc";
    public Developer Developer { get; set; }
    public Session Session { get; set; }

    public SmiteClient(Developer developer, Session session = null) : base()
    {
        BaseAddress = new(Endpoint);
        Developer = developer;
        Session = session;
        DefaultRequestVersion = HttpVersion.Version30;
        DefaultVersionPolicy = HttpVersionPolicy.RequestVersionOrLower;
    }

    public async Task<Session> CreateSession()
    {
        string url = BuildCreateSessionUrl(Developer, Endpoint);

        Console.WriteLine($"CreateSession: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"CreateSession: {(int)response.StatusCode} {response.ReasonPhrase}");
        Console.WriteLine(content);

        return JsonConvert.DeserializeObject<Session>(content);
    }

    public async Task<bool> TestSession()
    {
        string url = BuildTestSessionUrl(Developer, Session, Endpoint);

        Console.WriteLine($"TestSession: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"TestSession: {(int)response.StatusCode} {response.ReasonPhrase}");
        Console.WriteLine(content);

        if (content == null)
        {
            return false;
        }

        return content.Contains("This was a successful test");
    }


    public async Task<object> GetDataUsed()
    {
        string url = BuildGetDataUsedUrl(Developer, Session, Endpoint);

        Console.WriteLine($"GetDataUsed: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetDataUsed: {(int)response.StatusCode} {response.ReasonPhrase}");
        Console.WriteLine(content);

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }

        return JsonConvert.DeserializeObject<object>(content, JsonController.SerializerSettings);
    }

    public async Task<List<God>> GetGods(string language)
    {
        string url = BuildGetGodsUrl(Developer, Session, Endpoint, language);

        Console.WriteLine($"GetGods: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetGods: {(int)response.StatusCode} {response.ReasonPhrase}");

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return new();
        }

        return JsonConvert.DeserializeObject<List<God>>(content, JsonController.SerializerSettings);
    }

    public async Task<List<Item>> GetItems(string language)
    {
        string url = BuildGetItemsUrl(Developer, Session, Endpoint, language);

        Console.WriteLine($"GetItems: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetItems: {(int)response.StatusCode} {response.ReasonPhrase}");

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return new();
        }

        return JsonConvert.DeserializeObject<List<Item>>(content, JsonController.SerializerSettings);
    }

    public async Task<List<MatchPlayer>> GetMatchDetails(string matchID)
    {
        string url = BuildGetMatchDetailsUrl(Developer, Session, Endpoint, matchID);

        Console.WriteLine($"GetMatchDetails: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetMatchDetails: {(int)response.StatusCode} {response.ReasonPhrase}");

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return new();
        }

        return JsonConvert.DeserializeObject<List<MatchPlayer>>(content, JsonController.SerializerSettings);
    }

    public async Task<List<MatchPlayer>> GetMatchDetailsBatch(List<string> matchIds)
    {
        int retries = 5;
        string url = BuildGetMatchDetailsBatchUrl(Developer, Session, Endpoint, matchIds);

        Console.WriteLine($"GetMatchDetailsBatch: {url}");
        HttpResponseMessage response = await GetAsync(url);

        for (int i = 0; i < retries && response.StatusCode != HttpStatusCode.OK; i++)
        {
            Console.WriteLine($"GetMatchDetailsBatch: {url}");
            response = await GetAsync(url);
        }

        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetMatchDetailsBatch: {(int)response.StatusCode} {response.ReasonPhrase}");

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return new();
        }

        return JsonConvert.DeserializeObject<List<MatchPlayer>>(content, JsonController.SerializerSettings);
    }

    public async Task<List<Match>> GetMatchIdsByQueue(int queue, string date, string hour)
    {
        string url = BuildGetMatchIdsByQueueUrl(Developer, Session, Endpoint, queue, date, hour);

        Console.WriteLine($"GetMatchIdsByQueue: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetMatchIdsByQueue: {(int)response.StatusCode} {response.ReasonPhrase}");

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return new();
        }

        return JsonConvert.DeserializeObject<List<Match>>(content, JsonController.SerializerSettings);
    }

    public async Task<object> GetMotd()
    {
        string url = BuildGetMotdUrl(Developer, Session, Endpoint);

        Console.WriteLine($"GetMotd: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"GetMotd: {(int)response.StatusCode} {response.ReasonPhrase}");

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }

        return JsonConvert.DeserializeObject<object>(content, JsonController.SerializerSettings);
    }

    public async Task<string> Ping()
    {
        string url = BuildPingUrl(Endpoint);

        Console.WriteLine($"Ping: {url}");
        HttpResponseMessage response = await GetAsync(url);
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"Ping: {(int)response.StatusCode} {response.ReasonPhrase}");
        Console.WriteLine(content);

        return content;
    }
}
