﻿using System.Security.Cryptography;
using System.Text;

using static SmiteLogs.Utilities.Time;

namespace SmiteLogs.Models.SmiteApi;

public class Signature
{
    public string Method { get; set; }
    public string Timestamp { get; set; }
    private string _devID;
    private string _devAuthKey;

    public Signature(string devID, string method, string devAuthKey)
    {
        _devID = devID;
        Method = method;
        _devAuthKey = devAuthKey;
        Timestamp = GetTimestamp();
    }

    public string Generate()
    {
        MD5 md5 = MD5.Create();
        byte[] bytes = Encoding.UTF8.GetBytes($"{_devID}{Method}{_devAuthKey}{Timestamp}");
        bytes = md5.ComputeHash(bytes);
        StringBuilder builder = new();
        foreach (byte b in bytes)
        {
            builder.Append(b.ToString("x2").ToLower());
        }
        return builder.ToString();
    }
}
