﻿using SmiteLogs.Models.SmiteApi;

namespace SmiteLogs.Models;

public class Configuration
{
    public string Cache { get; set; }
    public string Site { get; set; }
    public int BatchSize { get; set; } = 20;
    public string Database { get; set; }
    public bool ExportJson { get; set; } = true;
    public Developer Developer { get; set; }
    public string LanguageCode { get; set; } = "1";
    public int QueueSampleSize { get; set; } = 2500;
    public Session Session { get; set; }

    public Configuration Clone()
    {
        return MemberwiseClone() as Configuration;
    }
}
