﻿using SmiteLogs.Models.Items;

namespace SmiteLogs.Models.Builds;

public class GodBuild
{
    public int GodId { get; set; }
    public int QueueId { get; set; }
    public string Patch { get; set; }
    public List<WeightedItem> Build { get; set; }
}
