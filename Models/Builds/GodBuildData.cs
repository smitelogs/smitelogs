﻿using SmiteLogs.Models.Items;

namespace SmiteLogs.Models.Builds;

public class GodBuildData
{
    public int GodId { get; set; }
    public DateRange DateRange { get; set; }
    public Dictionary<int, List<WeightedItem>> BuildsByQueue { get; set; }
}
