﻿namespace SmiteLogs.Models.Items;

public class WeightedItem : IEquatable<WeightedItem>
{
    public string Patch { get; set; }
    public int? GodId { get; set; }
    public int ItemId { get; set; }
    public int ItemSlot { get; set; }
    public string Name { get; set; }
    public int? QueueId { get; set; }
    public int ItemTier { get; set; }
    public int ChildItemId { get; set; }

    /// <summary>
    /// The number of times this item was purchased win or lose.
    /// </summary>
    public int Popularity { get; set; }

    /// <summary>
    /// The number matches played.
    /// </summary>
    public int TotalMatches { get; set; }
    public float WeightedWinRate { get; set; }
    public float WinRate { get; set; }
    public int Wins { get; set; }

    public void CalculateWinRate()
    {
        WinRate = (Wins / (float)Popularity) * 100;
        float mod = (Popularity / (float)TotalMatches);

        WeightedWinRate = WinRate * mod;
    }

    public bool Equals(WeightedItem other) => Patch == other.Patch && GodId == other.GodId && ItemId == other.ItemId && QueueId == other.QueueId;
    public override bool Equals(object obj) => Equals(obj as WeightedItem);
    public override int GetHashCode() => (Patch, GodId, ItemId, QueueId).GetHashCode();
}
