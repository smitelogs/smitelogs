﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Items;

public class AbilityDescription
{
    public string Description { get; set; }

    [JsonProperty("Menuitems")]
    public List<MenuItem> MenuItems { get; set; }

    public string SecondaryDescription { get; set; }
}
