﻿namespace SmiteLogs.Models.Items;

public class Stats
{
    public static readonly Dictionary<string, ItemStats> ItemStatsByName = new()
    {
        { "Attack Speed", ItemStats.AttackSpeed },
        { "Cooldown Reduction", ItemStats.CooldownReduction },
        { "Critical Strike Chance", ItemStats.CriticalStrikeChance },
        { "Crowd Control Reduction", ItemStats.CrowdControlReduction },
        { "Health", ItemStats.Health },
        { "HP5", ItemStats.HP5 },
        { "Magical Lifesteal", ItemStats.MagicalLifesteal },
        { "Magical Penetration", ItemStats.MagicalPenetration },
        { "Magical Power", ItemStats.MagicalPower },
        { "Magical Protection", ItemStats.MagicalProtection },
        { "Mana", ItemStats.Mana },
        { "Movement Speed", ItemStats.MovementSpeed },
        { "MP5", ItemStats.MP5 },
        { "Physical Lifesteal", ItemStats.PhysicalLifesteal },
        { "Physical Penetration", ItemStats.PhysicalPenetration },
        { "Physical Power", ItemStats.PhysicalPower },
        { "Physical Protection", ItemStats.PhysicalProtection }
    };

    public static readonly HashSet<ItemStats> IntegerStats = new()
    {
        ItemStats.Health,
        ItemStats.HP5,
        ItemStats.MagicalPenetration,
        ItemStats.MagicalPower,
        ItemStats.MagicalProtection,
        ItemStats.Mana,
        ItemStats.MP5,
        ItemStats.PhysicalPenetration,
        ItemStats.PhysicalPower,
        ItemStats.PhysicalProtection
    };

    public static readonly HashSet<ItemStats> RealStats = new()
    {
        ItemStats.AttackSpeed,
        ItemStats.CooldownReduction,
        ItemStats.CriticalStrikeChance,
        ItemStats.CrowdControlReduction,
        ItemStats.MagicalLifesteal,
        ItemStats.MagicalPenetrationPercent,
        ItemStats.MovementSpeed,
        ItemStats.PhysicalLifesteal,
        ItemStats.PhysicalPenetrationPercent,
    };
}
