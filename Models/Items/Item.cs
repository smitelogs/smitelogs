﻿using Newtonsoft.Json;
using SmiteLogs.Models.Gods;
using System.Text.RegularExpressions;

namespace SmiteLogs.Models.Items;

public class Item
{
    public string ActiveFlag { get; set; }
    public int ChildItemId { get; set; }
    public string DeviceName { get; set; }
    public int IconId { get; set; }
    public AbilityDescription ItemDescription { get; set; }
    public int ItemId { get; set; }
    public int ItemTier { get; set; }
    public int Price { get; set; }
    public string RestrictedRoles { get; set; }
    public int RootItemId { get; set; }
    public string ShortDesc { get; set; }
    public bool StartingItem { get; set; }
    public string Type { get; set; }

    [JsonProperty("itemIcon_URL")]
    public string ItemIconUrl { get; set; }

    [JsonProperty("ret_msg")]
    public string ReturnMessage { get; set; }

    // Item stats
    public float AttackSpeed { get; set; }
    public float CooldownReduction { get; set; }
    public float CriticalStrikeChance { get; set; }
    public float CrowdControlReduction { get; set; }
    public int Health { get; set; }
    public int HP5 { get; set; }
    public float MagicalLifesteal { get; set; }
    public int MagicalPenetration { get; set; }
    public float MagicalPenetrationPercent { get; set; }
    public int MagicalPower { get; set; }
    public int MagicalProtection { get; set; }
    public int Mana { get; set; }
    public float MovementSpeed { get; set; }
    public int MP5 { get; set; }
    public float PhysicalLifesteal { get; set; }
    public int PhysicalPenetration { get; set; }
    public float PhysicalPenetrationPercent { get; set; }
    public int PhysicalPower { get; set; }
    public int PhysicalProtection { get; set; }

    Regex regex = new(@"\<font color=\'(#[A-z0-9]*)\'\>(\w*)");


    public void BindFloatValue(string name, string value)
    {
        float valueAsFloat = ParsePercentAsFloat(value);

        switch (name)
        {
            case "AttackSpeed":
                AttackSpeed = valueAsFloat;
                return;

            case "CooldownReduction":
                CooldownReduction = valueAsFloat;
                return;

            case "CriticalStrikeChance":
                CriticalStrikeChance = valueAsFloat;
                return;

            case "CrowdControlReduction":
                CrowdControlReduction = valueAsFloat;
                return;

            case "MagicalLifesteal":
                MagicalLifesteal = valueAsFloat;
                return;

            case "MagicalPenetrationPercent":
                MagicalPenetrationPercent = valueAsFloat;
                return;

            case "MovementSpeed":
                MovementSpeed = valueAsFloat;
                return;

            case "PhysicalLifesteal":
                PhysicalLifesteal = valueAsFloat;
                return;

            case "PhysicalPenetrationPercent":
                PhysicalPenetrationPercent = valueAsFloat;
                return;

            default:
                break;
        }
    }

    public void BindIntValue(string name, string value)
    {
        int valueAsInt = int.Parse(value);

        switch (name)
        {
            case "Health":
                Health = valueAsInt;
                return;

            case "HP5":
                HP5 = valueAsInt;
                return;

            case "MagicalPower":
                MagicalPower = valueAsInt;
                return;

            case "MagicalPenetration":
                MagicalPenetration = valueAsInt;
                return;

            case "MagicalProtection":
                MagicalProtection = valueAsInt;
                return;

            case "Mana":
                Mana = valueAsInt;
                return;

            case "MP5":
                MP5 = valueAsInt;
                return;

            case "PhysicalPenetration":
                PhysicalPenetration = valueAsInt;
                return;

            case "PhysicalPower":
                PhysicalPower = valueAsInt;
                return;

            case "PhysicalProtection":
                PhysicalProtection = valueAsInt;
                return;

            default:
                break;
        }
    }

    public static string ConvertName(string value)
    {
        return value switch
        {
            "MagicalPenetration" => "MagicalPenetrationPercent",
            "PhysicalPenetration" => "PhysicalPenetrationPercent",
            _ => value,
        };
    }

    public static string NormalizeName(string value) => value.Replace(" ", "").Trim();

    public static string NormalizeValue(string value) => value.Replace("+", "").Trim();

    public void Parse()
    {
        foreach (MenuItem stat in ItemDescription.MenuItems)
        {
            string name = stat.Description;
            string value = stat.Value;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(value)) continue;

            name = NormalizeName(name);
            value = NormalizeValue(value);

            if (value.Contains('%'))
            {
                BindFloatValue(ConvertName(name), value);
            }
            else
            {
                BindIntValue(name, value);
            }
        }
    }

    public static float ParsePercentAsFloat(string value)
    {
        value = value.Replace("%", "");
        return MathF.Round(float.Parse(value) * 0.01f, 2);
    }

    public static Item operator +(Item item1, Item item2)
    {
        return new()
        {
            AttackSpeed = item1.AttackSpeed + item2.AttackSpeed,
            CooldownReduction = item1.CooldownReduction + item2.CooldownReduction,
            CriticalStrikeChance = item1.CriticalStrikeChance + item2.CriticalStrikeChance,
            CrowdControlReduction = item1.CrowdControlReduction + item2.CrowdControlReduction,
            Health = item1.Health + item2.Health,
            HP5 = item1.HP5 + item2.HP5,
            MagicalLifesteal = item1.MagicalLifesteal + item2.MagicalLifesteal,
            MagicalPenetration = item1.MagicalPenetration + item2.MagicalPenetration,
            MagicalPenetrationPercent = item1.MagicalPenetrationPercent + item2.MagicalPenetrationPercent,
            MagicalPower = item1.MagicalPower + item2.MagicalPower,
            MagicalProtection = item1.MagicalProtection + item2.MagicalProtection,
            Mana = item1.Mana + item2.Mana,
            MovementSpeed = item1.MovementSpeed + item2.MovementSpeed,
            MP5 = item1.MP5 + item2.MP5,
            PhysicalLifesteal = item1.PhysicalLifesteal + item2.PhysicalLifesteal,
            PhysicalPenetration = item1.PhysicalPenetration + item2.PhysicalPenetration,
            PhysicalPenetrationPercent = item1.PhysicalPenetrationPercent + item2.PhysicalPenetrationPercent,
            PhysicalPower = item1.PhysicalPower + item2.PhysicalPower,
            PhysicalProtection = item1.PhysicalProtection + item2.PhysicalProtection,
        };
    }

    public static Item operator -(Item item1, Item item2)
    {
        return new()
        {
            AttackSpeed = item1.AttackSpeed - item2.AttackSpeed,
            CooldownReduction = item1.CooldownReduction - item2.CooldownReduction,
            CriticalStrikeChance = item1.CriticalStrikeChance - item2.CriticalStrikeChance,
            CrowdControlReduction = item1.CrowdControlReduction - item2.CrowdControlReduction,
            Health = item1.Health - item2.Health,
            HP5 = item1.HP5 - item2.HP5,
            MagicalLifesteal = item1.MagicalLifesteal - item2.MagicalLifesteal,
            MagicalPenetration = item1.MagicalPenetration - item2.MagicalPenetration,
            MagicalPenetrationPercent = item1.MagicalPenetrationPercent - item2.MagicalPenetrationPercent,
            MagicalPower = item1.MagicalPower - item2.MagicalPower,
            MagicalProtection = item1.MagicalProtection - item2.MagicalProtection,
            Mana = item1.Mana - item2.Mana,
            MovementSpeed = item1.MovementSpeed - item2.MovementSpeed,
            MP5 = item1.MP5 - item2.MP5,
            PhysicalLifesteal = item1.PhysicalLifesteal - item2.PhysicalLifesteal,
            PhysicalPenetration = item1.PhysicalPenetration - item2.PhysicalPenetration,
            PhysicalPenetrationPercent = item1.PhysicalPenetrationPercent - item2.PhysicalPenetrationPercent,
            PhysicalPower = item1.PhysicalPower - item2.PhysicalPower,
            PhysicalProtection = item1.PhysicalProtection - item2.PhysicalProtection,
        };
    }

    public int GetPhysicalEffectiveHealth() => Health * (100 + PhysicalProtection) / 100;
    public int GetMagicalEffectiveHealth() => Health * (100 + MagicalProtection) / 100;
    public float GetPhysicalMitigation() => 1 - (100f / (100f + PhysicalProtection));
    public float GetMagicalMitigation() => 1 - (100f / (100f + MagicalProtection));

    public string ToTooltip()
    {
        List<string> stats = new();
        if (AttackSpeed != 0)                   stats.Add($"{(int)(AttackSpeed * 100f)}% Attack Speed");
        if (CooldownReduction != 0)             stats.Add($"{(int)(CooldownReduction * 100f)}% Cooldown Reduction");
        if (CriticalStrikeChance != 0)          stats.Add($"{(int)(CriticalStrikeChance * 100f)}% Critical Strike Chance");
        if (CrowdControlReduction != 0)         stats.Add($"{(int)(CrowdControlReduction * 100f)}% Crowd Control Reduction");
        if (Health != 0)                        stats.Add($"{Health} Health");
        if (HP5 != 0)                           stats.Add($"{HP5} HP5");
        if (MagicalLifesteal != 0)              stats.Add($"{(int)(MagicalLifesteal * 100f)}% Magical Lifesteal");
        if (MagicalPenetration != 0)            stats.Add($"{MagicalPenetration} Magical Penetration");
        if (MagicalPenetrationPercent != 0)     stats.Add($"{(int)(MagicalPenetrationPercent * 100f)}% Magical Penetration");
        if (MagicalPower != 0)                  stats.Add($"{MagicalPower} Magical Power");
        if (MagicalProtection != 0)             stats.Add($"{MagicalProtection} Magical Protection");
        if (Mana != 0)                          stats.Add($"{Mana} Mana");
        if (MovementSpeed != 0)                 stats.Add($"{(int)(MovementSpeed * 100f)}% Movement Speed");
        if (MP5 != 0)                           stats.Add($"{MP5} MP5");
        if (PhysicalLifesteal != 0)             stats.Add($"{(int)(PhysicalLifesteal * 100f)}% Physical Lifesteal");
        if (PhysicalPenetration != 0)           stats.Add($"{PhysicalPenetration} Physical Penetration");
        if (PhysicalPenetrationPercent != 0)    stats.Add($"{(int)(PhysicalPenetrationPercent * 100f)}% Physical Penetration");
        if (PhysicalPower != 0)                 stats.Add($"{PhysicalPower} Physical Power");
        if (PhysicalProtection != 0)            stats.Add($"{PhysicalProtection} Physical Protection");

        string tooltip = $"<h6>{DeviceName}</h6>";

        if (stats.Any())
        {
            tooltip += $"<p>{string.Join("<br>", stats.ToArray())}</p>";
        }

        if(ItemDescription?.SecondaryDescription != null)
        {
            tooltip += $"<p>{regex.Replace(ItemDescription.SecondaryDescription.Replace("<n>", "").Replace("GLYPH", "<br/>GLYPH"), $"<b>$2</b>")}";
        }

        return tooltip;
    }

    public string ToBuildSummary(God god, int level)
    {
        List<string> stats = new();

        int health = Health + god.Health + god.HealthPerLevel * level;
        int mana = Mana + god.Mana + god.ManaPerLevel * level;
        int hp5 = HP5 + (int)(god.HealthPerFive + god.HP5PerLevel * level);
        int mp5 = MP5 + (int)(god.ManaPerFive + god.MP5PerLevel * level);

        int physicalPower = PhysicalPower + (int)(god.PhysicalPower + god.PhysicalPowerPerLevel * level);
        int magicalPower = MagicalPower + (int)(god.MagicalPower + god.MagicalPowerPerLevel * level);
        int physicalProtection = PhysicalProtection + (int)(god.PhysicalProtection + god.PhysicalProtectionPerLevel * level);
        int magicalProtection = MagicalProtection + (int)(god.MagicalProtection + god.MagicalProtectionPerLevel * level);

        float attackSpeed = (god.AttackSpeed + god.AttackSpeedPerLevel * level) * (1 + AttackSpeed);
        float rawMovementSpeed = god.Speed * (1 + MovementSpeed);
        int movementSpeed = (int)(rawMovementSpeed <= 457f ? rawMovementSpeed : rawMovementSpeed <= 540.5f ? 457 + 0.8f * (rawMovementSpeed - 457) : 523.8f + 0.5f * (rawMovementSpeed - 540.5f));

        int physicalEffectiveHealth = health * (100 + physicalProtection) / 100;
        int magicalEffectiveHealth = health * (100 + magicalProtection) / 100;
        float physicalMitigation = 1 - (100f / (100f + physicalProtection));
        float magicalMitigation = 1 - (100f / (100f + magicalProtection));

        if (attackSpeed != 0)                   stats.Add($"{attackSpeed:0.00} Attack Speed");
        if (CooldownReduction != 0)             stats.Add($"{(int)(CooldownReduction * 100f)}% Cooldown Reduction");
        if (CriticalStrikeChance != 0)          stats.Add($"{(int)(CriticalStrikeChance * 100f)}% Critical Strike Chance");
        if (CrowdControlReduction != 0)         stats.Add($"{(int)(CrowdControlReduction * 100f)}% Crowd Control Reduction");
        if (health != 0)                        stats.Add($"{health} Health");
        if (hp5 != 0)                           stats.Add($"{hp5} HP5");
        if (MagicalLifesteal != 0)              stats.Add($"{(int)(MagicalLifesteal * 100f)}% Magical Lifesteal");
        if (MagicalPenetration != 0)            stats.Add($"{MagicalPenetration} Magical Penetration");
        if (MagicalPenetrationPercent != 0)     stats.Add($"{(int)(MagicalPenetrationPercent * 100f)}% Magical Penetration");
        if (magicalPower != 0)                  stats.Add($"{magicalPower} Magical Power");
        if (magicalProtection != 0)             stats.Add($"{magicalProtection} Magical Protection");
        if (mana != 0)                          stats.Add($"{mana} Mana");
        if (movementSpeed != 0)                 stats.Add($"{movementSpeed} Movement Speed");
        if (mp5 != 0)                           stats.Add($"{mp5} MP5");
        if (PhysicalLifesteal != 0)             stats.Add($"{(int)(PhysicalLifesteal * 100f)}% Physical Lifesteal");
        if (PhysicalPenetration != 0)           stats.Add($"{PhysicalPenetration} Physical Penetration");
        if (PhysicalPenetrationPercent != 0)    stats.Add($"{(int)(PhysicalPenetrationPercent * 100f)}% Physical Penetration");
        if (physicalPower != 0)                 stats.Add($"{physicalPower} Physical Power");
        if (physicalProtection != 0)            stats.Add($"{physicalProtection} Physical Protection");
        if (physicalEffectiveHealth != 0)       stats.Add($"{physicalEffectiveHealth} Physical Effective HP");
        if (magicalEffectiveHealth != 0)        stats.Add($"{magicalEffectiveHealth} Magical Effective HP");
        if (physicalMitigation != 0)            stats.Add($"{physicalMitigation * 100f:0.00}% Physical Mitigation");
        if (magicalMitigation != 0)             stats.Add($"{magicalMitigation * 100f:0.00}% Magical Mitigation");

        string tooltip = $"<h6>Level 20 Build Summary</h6>";

        if (stats.Any())
        {
            tooltip += $"<p>{string.Join("<br>", stats.ToArray())}</p>";
        }

        return tooltip;
    }
}


