﻿namespace SmiteLogs.Models.Items;

public class MenuItem
{
    public string Description { get; set; }
    public string Value { get; set; }
    public ItemStats DbName { get; set; }
    public object DbValue { get; set; }

    public bool Convert()
    {
        if (string.IsNullOrEmpty(Description) || string.IsNullOrEmpty(Value)) return false;

        IEnumerable<string> keys = Stats.ItemStatsByName.Keys.Where(k => k.Contains(Description));
        if (!keys.Any()) return false;

        string key = keys.First();
        DbName = Stats.ItemStatsByName[key];

        Value = Value.Replace("+", "").Trim();
        if (Value.Contains('%'))
        {
            switch (DbName)
            {
                case ItemStats.MagicalPenetration:
                    DbName = ItemStats.MagicalPenetrationPercent;
                    break;

                case ItemStats.PhysicalPenetration:
                    DbName = ItemStats.PhysicalPenetrationPercent;
                    break;
            }

            Value = Value.Replace("%", "");
            DbValue = MathF.Round(float.Parse(Value) * 0.01f, 2);
        }
        else
        {
            DbValue = int.Parse(Value);
        }

        return true;
    }
}
