﻿namespace SmiteLogs.Models;

public enum Queues
{
    Conquest = 426,
    Arena = 435,
    DuelRanked = 440,
    Assault = 445,
    Joust = 448,
    JoustRanked = 450,
    ConquestRanked = 451,
    Seige = 459,
    Clash = 466,
    DuelRankedController = 502,
    JoustRankedController = 503,
    ConquestRankedController = 504
}

public enum ItemStats
{
    AttackSpeed,
    CooldownReduction,
    CriticalStrikeChance,
    CrowdControlReduction,
    Health,
    HP5,
    MagicalLifesteal,
    MagicalPenetration,
    MagicalPenetrationPercent,
    MagicalPower,
    MagicalProtection,
    Mana,
    MovementSpeed,
    MP5,
    PhysicalLifesteal,
    PhysicalPenetration,
    PhysicalPenetrationPercent,
    PhysicalPower,
    PhysicalProtection,
}
