﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Matches;

public class Match
{
    [JsonProperty("Active_Flag")]
    public string ActiveFlag { get; set; }

    [JsonProperty("Match")]
    public string Id { get; set; }

    [JsonProperty("ret_msg")]
    public string ReturnMessage { get; set; }

    public string Date { get; set; }
    public int Queue { get; set; }
}
