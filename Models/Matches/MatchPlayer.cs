﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Matches;

public class MatchPlayer
{
    [JsonProperty("Account_Level")]
    public int AccountLevel { get; set; }

    public int ActiveId1 { get; set; }

    public int ActiveId2 { get; set; }

    public int ActiveId3 { get; set; }

    public int ActiveId4 { get; set; }

    public string ActivePlayerId { get; set; }

    public int Assists { get; set; }

    public string Ban1 { get; set; }

    public string Ban2 { get; set; }

    public string Ban3 { get; set; }

    public string Ban4 { get; set; }

    public string Ban5 { get; set; }

    public string Ban6 { get; set; }

    public string Ban7 { get; set; }

    public string Ban8 { get; set; }

    public string Ban9 { get; set; }

    public string Ban10 { get; set; }

    public string Ban11 { get; set; }

    public string Ban12 { get; set; }

    public int Ban1Id { get; set; }

    public int Ban2Id { get; set; }

    public int Ban3Id { get; set; }

    public int Ban4Id { get; set; }

    public int Ban5Id { get; set; }

    public int Ban6Id { get; set; }

    public int Ban7Id { get; set; }

    public int Ban8Id { get; set; }

    public int Ban9Id { get; set; }

    public int Ban10Id { get; set; }

    public int Ban11Id { get; set; }

    public int Ban12Id { get; set; }

    [JsonProperty("Camps_Cleared")]
    public int CampsCleared { get; set; }

    [JsonProperty("Conquest_Losses")]
    public int ConquestLosses { get; set; }

    [JsonProperty("Conquest_Points")]
    public int ConquestPoints { get; set; }

    [JsonProperty("Conquest_Tier")]
    public int ConquestTier { get; set; }

    [JsonProperty("Conquest_Wins")]
    public int ConquestWins { get; set; }

    [JsonProperty("Damage_Bot")]
    public int DamageBot { get; set; }

    [JsonProperty("Damage_Done_In_Hand")]
    public int DamageDoneInHand { get; set; }

    [JsonProperty("Damage_Done_Magical")]
    public int DamageDoneMagical { get; set; }

    [JsonProperty("Damage_Done_Physical")]
    public int DamageDonePhysical { get; set; }

    [JsonProperty("Damage_Mitigated")]
    public int DamageMitigated { get; set; }

    [JsonProperty("Damage_Player")]
    public int DamagePlayer { get; set; }

    [JsonProperty("Damage_Taken")]
    public int DamageTaken { get; set; }

    [JsonProperty("Damage_Taken_Magical")]
    public int DamageTakenMagical { get; set; }

    [JsonProperty("Damage_Taken_Physical")]
    public int DamageTakenPhysical { get; set; }

    public int Deaths { get; set; }

    [JsonProperty("Distance_Traveled")]
    public int DistanceTraveled { get; set; }

    [JsonProperty("Duel_Losses")]
    public int DuelLosses { get; set; }

    [JsonProperty("Duel_Points")]
    public int DuelPoints { get; set; }

    [JsonProperty("Duel_Tier")]
    public int DuelTier { get; set; }

    [JsonProperty("Duel_Wins")]
    public int DuelWins { get; set; }

    [JsonProperty("Entry_Datetime")]
    public string EntryDatetime { get; set; }

    [JsonProperty("Final_Match_Level")]
    public int FinalMatchLevel { get; set; }

    [JsonProperty("First_Ban_Side")]
    public string FirstBanSide { get; set; }

    public int GodId { get; set; }

    [JsonProperty("Gold_Earned")]
    public int GoldEarned { get; set; }

    [JsonProperty("Gold_Per_Minute")]
    public int GoldPerMinute { get; set; }

    public int Healing { get; set; }

    [JsonProperty("Healing_Bot")]
    public int HealingBot { get; set; }

    [JsonProperty("Healing_Player_Self")]
    public int HealingPlayerSelf { get; set; }

    public int ItemId1 { get; set; }

    public int ItemId2 { get; set; }

    public int ItemId3 { get; set; }

    public int ItemId4 { get; set; }

    public int ItemId5 { get; set; }

    public int ItemId6 { get; set; }

    [JsonProperty("Item_Active_1")]
    public string ItemActive1 { get; set; }

    [JsonProperty("Item_Active_2")]
    public string ItemActive2 { get; set; }

    [JsonProperty("Item_Active_3")]
    public string ItemActive3 { get; set; }

    [JsonProperty("Item_Active_4")]
    public string ItemActive4 { get; set; }

    [JsonProperty("Item_Purch_1")]
    public string ItemPurchase1 { get; set; }

    [JsonProperty("Item_Purch_2")]
    public string ItemPurchase2 { get; set; }

    [JsonProperty("Item_Purch_3")]
    public string ItemPurchase3 { get; set; }

    [JsonProperty("Item_Purch_4")]
    public string ItemPurchase4 { get; set; }

    [JsonProperty("Item_Purch_5")]
    public string ItemPurchase5 { get; set; }

    [JsonProperty("Item_Purch_6")]
    public string ItemPurchase6 { get; set; }

    [JsonProperty("Joust_Losses")]
    public int JoustLosses { get; set; }

    [JsonProperty("Joust_Points")]
    public int JoustPoints { get; set; }

    [JsonProperty("Joust_Tier")]
    public int JoustTier { get; set; }

    [JsonProperty("Joust_Wins")]
    public int JoustWins { get; set; }

    [JsonProperty("Killing_Spree")]
    public int KillingSpree { get; set; }

    [JsonProperty("Kills_Bot")]
    public int KillsBot { get; set; }

    [JsonProperty("Kills_Double")]
    public int KillsDouble { get; set; }

    [JsonProperty("Kills_Fire_Giant")]
    public int KillsFireGiant { get; set; }

    [JsonProperty("Kills_First_Blood")]
    public int KillsFirstBlood { get; set; }

    [JsonProperty("Kills_Gold_Fury")]
    public int KillsGoldFury { get; set; }

    [JsonProperty("Kills_Penta")]
    public int KillsPenta { get; set; }

    [JsonProperty("Kills_Phoenix")]
    public int KillsPhoenix { get; set; }

    [JsonProperty("Kills_Player")]
    public int KillsPlayer { get; set; }

    [JsonProperty("Kills_Quadra")]
    public int KillsQuadra { get; set; }

    [JsonProperty("Kills_Seige_Juggernaut")]
    public int KillsSeigeJuggernaut { get; set; }

    [JsonProperty("Kills_Single")]
    public int KillsSingle { get; set; }

    [JsonProperty("Kills_Triple")]
    public int KillsTriple { get; set; }

    [JsonProperty("Kills_Wild_Juggernaut")]
    public int KillsWildJuggernaut { get; set; }

    [JsonProperty("Map_Game")]
    public string MapGame { get; set; }

    [JsonProperty("Mastery_Level")]
    public int MasteryLevel { get; set; }

    public long Match { get; set; }

    [JsonProperty("Match_Duration")]
    public int MatchDuration { get; set; }

    public List<MergedPlayer> MergedPlayers { get; set; }

    public int Minutes { get; set; }

    [JsonProperty("Multi_kill_Max")]
    public int MultiKillMax { get; set; }

    [JsonProperty("Objective_Assists")]
    public int ObjectiveAssists { get; set; }

    public int PartyId { get; set; }

    [JsonProperty("Rank_Stat_Conquest")]
    public float RankStatConquest { get; set; }

    [JsonProperty("Rank_Stat_Duel")]
    public float RankStatDuel { get; set; }

    [JsonProperty("Rank_Stat_Joust")]
    public float RankStatJoust { get; set; }

    [JsonProperty("Reference_Name")]
    public string ReferenceName { get; set; }

    public string Region { get; set; }

    public string Role { get; set; }

    public string Skin { get; set; }

    public int SkinId { get; set; }

    [JsonProperty("Structure_Damage")]
    public int StructureDamage { get; set; }

    public int Surrendered { get; set; }

    public int TaskForce { get; set; }

    public int Team1Score { get; set; }

    public int Team2Score { get; set; }

    public int TeamId { get; set; }

    [JsonProperty("Team_Name")]
    public string TeamName { get; set; }

    [JsonProperty("Time_Dead_Seconds")]
    public int TimeDeadSeconds { get; set; }

    [JsonProperty("Time_In_Match_Seconds")]
    public int TimeInMatchSeconds { get; set; }

    [JsonProperty("Towers_Destroyed")]
    public int TowersDestroyed { get; set; }

    [JsonProperty("Wards_Placed")]
    public int WardsPlaced { get; set; }

    [JsonProperty("Win_Status")]
    public string WinStatus { get; set; }

    [JsonProperty("Winning_TaskForce")]
    public int WinningTaskForce { get; set; }

    [JsonProperty("hasReplay")]
    public string HasReplay { get; set; }

    [JsonProperty("hz_gamer_tag")]
    public string HiRezGamerTag { get; set; }

    [JsonProperty("hz_player_name")]
    public string HiRezPlayerName { get; set; }

    [JsonProperty("match_queue_id")]
    public int MatchQueueId { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("playerId")]
    public string PlayerId { get; set; }

    [JsonProperty("playerName")]
    public string PlayerName { get; set; }

    [JsonProperty("playerPortalId")]
    public string PlayerPortalId { get; set; }

    [JsonProperty("playerPortalUserId")]
    public string PlayerPortalUserId { get; set; }

    [JsonProperty("ret_msg")]
    public string ReturnMessage { get; set; }
}
