﻿using Newtonsoft.Json;

namespace SmiteLogs.Models.Matches;

public class MergedPlayer
{
    [JsonProperty("merge_datetime")]
    public string MergeDatetime { get; set; }

    [JsonProperty("playerId")]
    public string PlayerId { get; set; }

    [JsonProperty("portalId")]
    public string PortalId { get; set; }
}
