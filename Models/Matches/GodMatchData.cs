﻿namespace SmiteLogs.Models.Matches;

public class GodMatchData
{
    public int GodId { get; set; }
    public DateRange DateRange { get; set; }
    public Dictionary<int, float> WinRatesByQueue { get; set; }
}
