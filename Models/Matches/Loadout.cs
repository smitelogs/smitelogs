﻿namespace SmiteLogs.Models.Matches;

public class Loadout
{
    public int? ActiveId1 { get; set; }
    public int? ActiveId2 { get; set; }
    public int? ActiveId3 { get; set; }
    public int? ActiveId4 { get; set; }
    public int? ItemId1 { get; set; }
    public int? ItemId2 { get; set; }
    public int? ItemId3 { get; set; }
    public int? ItemId4 { get; set; }
    public int? ItemId5 { get; set; }
    public int? ItemId6 { get; set; }

    public override string ToString()
    {
        string actives = "";
        string items = "";

        if (ItemId1 != null || ItemId2 != null || ItemId3 != null || ItemId4 != null || ItemId5 != null || ItemId6 != null)
        {
            items = $"Items({ItemId1}, {ItemId2}, {ItemId3}, {ItemId4}, {ItemId5}, {ItemId6})";
        }

        if (ActiveId1 != null || ActiveId2 != null || ActiveId3 != null || ActiveId4 != null)
        {
            actives = $" Actives({ActiveId1}, {ActiveId2}, {ActiveId3}, {ActiveId4})";
        }
        return $"{items}{actives}";
    }
}
