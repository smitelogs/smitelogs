﻿using Newtonsoft.Json;

namespace SmiteLogs.Models;

public class Motd
{
    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("gameMode")]
    public string GameMode { get; set; }

    [JsonProperty("maxPlayers")]
    public string MaxPlayers { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("ret_msg")]
    public string ReteurnMessage { get; set; }

    [JsonProperty("startDateTime")]
    public string StartDateTime { get; set; }

    [JsonProperty("team1GodsCSV")]
    public string Team1GodsCSV { get; set; }

    [JsonProperty("team2GodsCSV")]
    public string Team2GodsCSV { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }
}
