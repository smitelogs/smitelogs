﻿using System.Text;

namespace SmiteLogs.Models.Webpage;

public class Table
{
    public List<string> HeaderRow { get; set; }

    public List<string> HeaderColumn { get; set; }

    public List<List<string>> Values { get; set; }

    public new string ToString()
    {
        StringBuilder builder = new();
        builder.Append("<table class='table table-dark table-striped table-bordered sortable'>");

        builder.Append(BuildTableHeader(HeaderRow));
        builder.Append(BuildTableBody(HeaderColumn, Values));

        builder.Append("</table>");
        return builder.ToString();
    }

    private static string BuildTableHeader(List<string> columns)
    {
        StringBuilder builder = new();
        builder.Append("<thead><tr>");

        // Queues
        foreach (string column in columns)
        {
            builder.Append($"<th>{column}</th>");
        }

        builder.Append("</tr></thead>");
        return builder.ToString();
    }

    private static string BuildTableBody(List<string> headerColumn, List<List<string>> values)
    {
        StringBuilder builder = new();
        builder.Append("<tbody>");

        for (int i = 0; i < values.Count; i++)
        {
            List<string> row = values[i];
            row.Insert(0, headerColumn[i]);

            builder.Append("<tr>");

            foreach (string column in row)
            {
                builder.Append($"<td>{column}</td>");
            }

            builder.Append("</tr>");
        }

        builder.Append("</tbody>");
        return builder.ToString();
    }

    private static string BuildTableFooter(List<string> columns)
    {
        StringBuilder builder = new();

        builder.Append("<tfoot>");

        foreach (string column in columns)
        {
            builder.Append($"<tr>{column}</tr>");
        }

        builder.Append("</tfoot>");
        return builder.ToString();
    }
}
