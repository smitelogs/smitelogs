﻿using SmiteLogs.Dao;
using System.Text;

namespace SmiteLogs.Models.Webpage;

public class GodWinRates : Page
{
    private string Patch { get; set; }

    public GodWinRates(string table, string patch)
    {
        Patch = patch;

        string headerTemplate = Path.Combine(Config.Site, "templates", "header.xml");
        string bodyTemplate = Path.Combine(Config.Site, "templates", "body-gods.xml");
        string navbarTemplate = Path.Combine(Config.Site, "templates", "navbar-gods.xml");
        LoadTemplates(headerTemplate, bodyTemplate, navbarTemplate);

        Header.Parameters = new()
        {
            { "$stylesheet", "<link rel=\"stylesheet\" href=\"./css/styles.css?v=1.0\">" }
        };

        Body.Parameters = new()
        {
            { "$scripts", "./js" },
            { "$navbar", Navbar.Content },
            { "$pageTitle", "God Win Rates by Queue" },
            { "$samples", $"{new MatchPlayerDao(Config.Database).GetCount(MatchDateRange)}" },
            { "$dateRange", $"{MatchDateRange.FromWeb} - {MatchDateRange.ToWeb}" },
            { "$winrateTable", table }
        };
    }

    public new string ToString()
    {
        StringBuilder builder = new();

        builder.Append("<!doctype html>");
        builder.Append("<html lang=\"en\">");
        builder.Append(Header.Content);
        builder.Append(Body.Content);
        builder.Append("</html>");

        return builder.ToString();
    }

    public void Export()
    {
        BindParameters();

        //"gods", "winrates", Patch,
        string path = Path.Combine(Config.Site, "index.html");

        if (File.Exists(path))
        {
            File.Delete(path);
        }
        else
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        File.WriteAllText(path, ToString());
    }
}
