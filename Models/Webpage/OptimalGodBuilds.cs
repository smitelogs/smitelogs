﻿using SmiteLogs.Dao;
using System.Text;

namespace SmiteLogs.Models.Webpage;

public class OptimalGodBuilds : Page
{
    private int GodId { get; set; }
    private string Patch { get; set; }

    public OptimalGodBuilds(int godId, string patch, string table)
    {
        GodId = godId;
        Patch = patch;

        string headerTemplate = Path.Combine(Config.Site, "templates", "header.xml");
        string bodyTemplate = Path.Combine(Config.Site, "templates", "body-builds.xml");
        string navbarTemplate = Path.Combine(Config.Site, "templates", "navbar-builds.xml");
        LoadTemplates(headerTemplate, bodyTemplate, navbarTemplate);

        Header.Parameters = new()
        {
            { "$stylesheet", "<link rel=\"stylesheet\" href=\"../../../../css/styles.css?v=1.0\"" }
        };

        Body.Parameters = new()
        {
            { "$scripts", "../../../../js" },
            { "$navbar", Navbar.Content },
            { "$pageTitle", $"Optimal {GodsById[godId].Name} Builds" },
            { "$samples", $"{new MatchPlayerDao(Config.Database).GetCount(MatchDateRange, godId)}"},
            { "$dateRange", $"{MatchDateRange.FromWeb} - {MatchDateRange.ToWeb}" },
            { "$buildTable", table }
        };
    }

    public new string ToString()
    {
        StringBuilder builder = new();

        builder.Append("<!doctype html>");
        builder.Append("<html lang=\"en\">");
        builder.Append(Header.Content);
        builder.Append(Body.Content);
        builder.Append("</html>");

        return builder.ToString();
    }

    public void Export()
    {
        BindParameters();

        string godName = GodsById[GodId].Name.ToLower().Replace(' ', '-').Replace('\'', '-');
        string path = Path.Combine(Config.Site, "builds", "optimal", Patch, godName, $"index.html");

        if (File.Exists(path))
        {
            File.Delete(path);
        }
        else
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        File.WriteAllText(path, ToString());
    }
}
