﻿namespace SmiteLogs.Models.Webpage;

public class Component
{
    public Dictionary<string, string> Parameters { get; set; } = new();
    public string Content { get; set; }

    public void BindParameters()
    {
        if (Content == null) return;

        foreach(string key in Parameters.Keys)
        {
            Content = Content.Replace(key, Parameters[key]);
        }
    }

    public void LoadTemplate(string templatePath)
    {
        if (!File.Exists(templatePath)) return;
        Content = File.ReadAllText(templatePath);
    }
}
