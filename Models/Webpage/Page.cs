﻿namespace SmiteLogs.Models.Webpage;

public abstract class Page
{
    public Component Header { get; set; } = new();
    public Component Body { get; set; } = new();
    public Component Navbar { get; set; } = new();

    public void LoadTemplates(string headerTemplate, string bodyTemplate, string navbarTemplate)
    {
        Header.LoadTemplate(headerTemplate);
        Navbar.LoadTemplate(navbarTemplate);
        Body.LoadTemplate(bodyTemplate);
    }

    public void BindParameters()
    {
        Header.BindParameters();
        Navbar.BindParameters();
        Body.BindParameters();
    }
}
