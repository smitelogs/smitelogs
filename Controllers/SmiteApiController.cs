﻿using SmiteLogs.Dao;
using SmiteLogs.Models;
using SmiteLogs.Models.Gods;
using SmiteLogs.Models.Items;
using SmiteLogs.Models.Matches;
using SmiteLogs.Models.SmiteApi;

namespace SmiteLogs.Controllers;

public class SmiteApiController
{
    public static async Task<bool> CheckSession(Configuration config)
    {
        if (config.Session == null || config.Session.IsExpired())
        {
            Session session = await Client.CreateSession();
            config.Session = session;
            ConfigurationManager.Save(config);
        }

        if (config.Session?.Id == string.Empty)
        {
            Console.WriteLine(config.Session.Result);
            return false;
        }

        Client.Session = config.Session;

        return true;
    }

    public static void GetDataUsed()
    {
        Client.GetDataUsed().GetAwaiter().GetResult();
    }

    public static async Task GetGods(Configuration config)
    {
        List<God> gods = await Client.GetGods(config.LanguageCode);
        GodDao godDao = new(config.Database);

        if (gods.Count > 0)
        {
            godDao.DeleteGods();
        }

        godDao.BulkInsertGods(gods);
        if (gods != null)
        {
            JsonController.SaveAsJson(Path.Combine(config.Cache, "gods.json"), gods, config.ExportJson);
        }
    }

    public static async Task GetItems(Configuration config)
    {
        List<Item> items = await Client.GetItems(config.LanguageCode);
        ItemDao itemDao = new(config.Database);

        if(items.Count > 0)
        {
            itemDao.DeleteItems();
        }

        itemDao.BulkInsertItems(items);
        if (items != null)
        {
            JsonController.SaveAsJson(Path.Combine(config.Cache, "items.json"), items, config.ExportJson);
        }
    }

    public static async Task BulkProcessMatchesByQueue(int queue, string date)
    {
        try
        {
            MatchDao matchDao = new(Config.Database);
            await GetMatchIdsByQueue(queue, date);

            // Get the matches by date.
            List<string> matches = matchDao
                .SelectQueueMatchesByDate(queue, date)
                .Where(m => m.ActiveFlag.Equals("n"))
                .Select(m => m.Id)
                .ToList();

            // Select some samples from the full match list.
            int nth = Math.Clamp(matches.Count / Config.QueueSampleSize, 1, int.MaxValue);
            List<string> samples = matches
                .Where((x, i) => i % nth == 0)
                .Take(Config.QueueSampleSize)
                .ToList();

            List<MatchPlayer> players = new();

            List<Task> tasks = new();
            for (int i = 0; i < samples.Count; i += Config.BatchSize)
            {
                if(!await CheckSession(Config))
                {
                    continue;
                }

                Console.WriteLine($"Processing {QueuesById[queue]} queue for {date}, {i}/{samples.Count}");
                List<string> batch = samples
                    .Skip(i)
                    .Take(Config.BatchSize)
                    .ToList();

                tasks.Add(Task.Run(() => GetMatchDetailsBatch(batch)));

                // We have to throttle our query speed or else Smite API will return a 504.
                Thread.Sleep(1050);
            }

            Task t = Task.WhenAll(tasks.ToArray());
            t.Wait();

            foreach (Task<List<MatchPlayer>> task in tasks)
            {
                players.AddRange(task.Result);
            }

            MatchPlayerDao matchPlayerDao = new(Config.Database);
            matchPlayerDao.BulkInsertMatchPlayers(players);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }

    public static async Task GetAllMatchIdsByDate(params string[] dates)
    {
        if (!dates.Any())
        {
            dates = new string[] { DateTime.UtcNow.ToString("yyyy-MM-dd") };
        }

        foreach (string date in dates)
        {
            foreach (int queue in QueuesById.Keys)
            {
                await BulkProcessMatchesByQueue(queue, date);
            }
        }
    }

    public static async Task GetMatchDetails(string matchId)
    {
        List<MatchPlayer> players = await Client.GetMatchDetails(matchId);

        if (players == null) return;

        JsonController.SaveAsJson(Path.Combine(Config.Cache, "Matches", "Ids", $"{matchId}.json"), players, Config.ExportJson);

        MatchPlayerDao matchPlayerDao = new(Config.Database);
        matchPlayerDao.BulkInsertMatchPlayers(players);
    }

    public static async Task<List<MatchPlayer>> GetMatchDetailsBatch(List<string> matchIds)
    {
        List<MatchPlayer> players = await Client.GetMatchDetailsBatch(matchIds);

        if (players == null) return new();

        foreach (IGrouping<long, MatchPlayer> matchPlayers in players.Where(p => p != null).GroupBy(p => p.Match))
        {
            JsonController.SaveAsJson(Path.Combine(Config.Cache, "Matches", "Ids", $"{matchPlayers.Key}.json"), players, Config.ExportJson);
        }

        return players;
    }

    public static async Task GetMatchIdsByQueue(int queue, string date = null, string hour = "-1")
    {
        if (date == null)
        {
            date = DateTime.UtcNow.ToString("yyyy-MM-dd");
        }

        List<Match> matches = await Client.GetMatchIdsByQueue(queue, date, hour);

        if (matches == null) return;

        JsonController.SaveAsJson(Path.Combine(Config.Cache, "Matches", "Queues", $"{QueuesById[queue]}", $"{date}.json"), matches, Config.ExportJson);

        MatchDao matchDao = new(Config.Database);
        matchDao.BulkInsertMatches(matches, queue, date);
    }
}
