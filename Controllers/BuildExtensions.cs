﻿using SmiteLogs.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmiteLogs.Controllers;

public static class BuildExtensions
{
    public static IEnumerable<int> ListItemIDsByTier(this IEnumerable<WeightedItem> items, int tier) => items.Where(i => i.ItemTier == tier).Select(i => i.ItemId);
    public static IEnumerable<int> ListChildItemIDsByTier(this IEnumerable<WeightedItem> items, int tier) => items.Where(i => i.ItemTier == tier).Select(i => i.ChildItemId);
}
