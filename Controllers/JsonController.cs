﻿using Newtonsoft.Json;
using SmiteLogs.Models;
using SmiteLogs.Models.Builds;
using SmiteLogs.Models.Matches;

namespace SmiteLogs.Controllers;

public class JsonController
{

    public static readonly JsonSerializerSettings SerializerSettings = new()
    {
        Formatting = Formatting.None,
        NullValueHandling = NullValueHandling.Include,
        DateFormatString = DateRange.MATCH_FORMAT,
    };

    public static List<GodMatchData> ImportGodWinRates(string patch)
    {
        string path = Path.Combine(Config.Site, "gods", "winrates", patch, "data.json");

        if (!File.Exists(path))
        {
            Console.WriteLine($"{path} does not exist");
            return null;
        }

        return JsonConvert.DeserializeObject<List<GodMatchData>>(File.ReadAllText(path));
    }

    public static void ExportGodWinRates(string patch, object data)
    {
        string path = Path.Combine(Config.Site, "gods", "winrates", patch, "data.json");
        SaveAsJson(path, data, true);
    }

    public static GodBuildData ImportOptimalBuild(int godId, string patch)
    {
        patch ??= Patches.Keys.First();
        string godName = GodsById[godId].Name.ToLower().Replace(' ', '-').Replace('\'', '-');
        string path = Path.Combine(Config.Site, "builds", "optimal", patch, godName, "data.json");

        if (!File.Exists(path))
        {
            Console.WriteLine($"{path} does not exist");
            return null;
        }

        return JsonConvert.DeserializeObject<GodBuildData>(File.ReadAllText(path));
    }

    public static void ExportOptimalBuild(int godId, string patch, object data)
    {
        patch ??= Patches.Keys.First();
        string godName = GodsById[godId].Name.ToLower().Replace(' ', '-').Replace('\'', '-');
        string path = Path.Combine(Config.Site, "builds", "optimal", patch, godName, "data.json");
        SaveAsJson(path, data, true);
    }

    public static List<MatchPlayer> LoadMatchDetailsById(string matchId)
    {
        string path = Path.Combine(Config.Cache, "Matches", "Ids", $"{matchId}.json");

        if (!File.Exists(path))
        {
            Console.WriteLine($"{path} does not exist");
            return null;
        }

        return JsonConvert.DeserializeObject<List<MatchPlayer>>(File.ReadAllText(path));
    }

    public static List<Match> LoadMatchesByDate(int queue, string date = null)
    {
        if (date == null)
        {
            date = DateTime.UtcNow.ToString("yyyy-MM-dd");
        }

        string path = Path.Combine(Config.Cache, "Matches", "Queues", $"{QueuesById[queue]}", $"{date}.json");

        if (!File.Exists(path))
        {
            Console.WriteLine($"{path} does not exist");
            return null;
        }

        return JsonConvert.DeserializeObject<List<Match>>(File.ReadAllText(path));
    }

    public static T LoadFromJson<T>(string path)
    {
        try
        {
            string json = File.ReadAllText(path);
            T data = JsonConvert.DeserializeObject<T>(json, SerializerSettings);
            Console.WriteLine($"Data loaded from: {path}");
            return data;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return default;
    }

    public static bool SaveAsJson(string path, object data, bool export = true)
    {
        if (!export) return false;

        if (File.Exists(path))
        {
            File.Delete(path);
        }
        else
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        try
        {
            string json = JsonConvert.SerializeObject(data, SerializerSettings);
            File.WriteAllText(path, json);
            Console.WriteLine($"Data saved to: {path}");
            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return false;
    }
}
