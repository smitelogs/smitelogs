﻿using SmiteLogs.Dao;
using SmiteLogs.Models.Builds;
using SmiteLogs.Models.Gods;
using SmiteLogs.Models.Items;
using SmiteLogs.Models.Webpage;

namespace SmiteLogs.Controllers;

public class BuildController
{
    public static List<GodBuildData> GetAllOptimalGodBuilds(string patch = null)
    {
        List<GodBuildData> optimalGodBuilds = new();

        List<Task> taskList = new();

        foreach (int godId in GodsById.Keys.Take(2))
        {
            taskList.Add(Task.Run(() => GetOptimalGodBuildByPatch(godId, patch)));
        }

        Task t = Task.WhenAll(taskList.ToArray());
        t.Wait();

        foreach (Task<GodBuildData> task in taskList)
        {
            optimalGodBuilds.Add(task.Result);
        }

        return optimalGodBuilds;
    }

    public static GodBuildData GetOptimalGodBuildByPatch(int godId, string patch = null)
    {
        patch ??= Patches.Keys.First();

        GodBuildData godBuildData = new()
        {
            GodId = godId,
            DateRange = Patches[patch],
            BuildsByQueue = new()
        };

        List<Task> taskList = new();

        foreach (int queue in QueuesById.Keys)
        {
            taskList.Add(Task.Run(() => GetOptimalBuild(godId, queue, patch)));
        }

        Task t = Task.WhenAll(taskList.ToArray());
        t.Wait();

        foreach (Task<GodBuild> task in taskList)
        {
            GodBuild godBuild = task.Result;
            godBuildData.BuildsByQueue.Add(godBuild.QueueId, godBuild.Build);
        }

        return godBuildData;
    }

    public static GodBuild GetOptimalBuild(int godId, int queue, string patch = null)
    {
        patch ??= Patches.Keys.First();
        Console.WriteLine($"Optimizing a {QueuesById[queue]} build for {GodsById[godId].Name}...");

        MatchPlayerDao matchPlayerDao = new(Config.Database);

        GodBuild optimalBuild = new()
        {
            GodId = godId,
            QueueId = queue,
            Patch = patch,
            Build = new()
        };

        for (int i = 1; i <= 6; i++)
        {
            List<WeightedItem> itemsByPopularity = matchPlayerDao.GetItemsByPopularity(i, godId, queue, patch);
            List<WeightedItem> itemsByWinRate = new();

            // Get win rates for 5 most popular first items
            foreach (WeightedItem item in itemsByPopularity.Take(5))
            {
                WeightedItem performance = matchPlayerDao.GetItemPerformance(item.ItemId, item.ItemSlot, item.GodId, item.QueueId, item.Patch);
                item.Wins = performance.Wins;
                item.Popularity = performance.Popularity;
                item.TotalMatches = performance.TotalMatches;
                item.CalculateWinRate();

                itemsByWinRate.Add(item ?? new());
            }

            // Ignore duplicates.
            WeightedItem bestItem = itemsByWinRate
                .Except(optimalBuild.Build) // Exclude duplicates.
                .Where(item =>

                    // Exclude tier 4 items that branch from the same tier 3 items. (Can't build Jotunn's Vigor after Jotunn's Ferocity)
                    !optimalBuild.Build.ListChildItemIDsByTier(4).Contains(item.ChildItemId) &&

                    // Exclude tier 4 items that branch from already built tier 3 items. (Can't build Jotunn's Vigor after Jotunn's Wrath)
                    !optimalBuild.Build.ListItemIDsByTier(3).Contains(item.ChildItemId) &&

                    // Exclude tier 3 items that have tier 4 versions already built. (Can't build Jotunn's Wrath after Jotunn's Vigor)
                    !optimalBuild.Build.ListChildItemIDsByTier(4).Contains(item.ItemId))

                .OrderByDescending(item => item.WinRate)
                .FirstOrDefault();

            float progress = (++ItemProgress * 100f) / (GodsById.Count() * QueuesById.Count() * 6);
            Console.WriteLine($"{bestItem?.Name} is the optimal slot {i} item for {GodsById[godId].Name} in {QueuesById[queue]} ({progress:0.00} %)");
            optimalBuild.Build.Add(bestItem);
        }

        return optimalBuild;
    }

    
    public static Table GetOptimalGodBuildTable(GodBuildData optimalGodBuildsByDate)
    {
        List<string> headerRow = new()
        {
            "Queue",
            "Item 1",
            "Item 2",
            "Item 3",
            "Item 4",
            "Item 5",
            "Item 6",
            "Build Summary"
        };

        List<string> headerColumn = optimalGodBuildsByDate
            .BuildsByQueue
            .Select(b => QueuesById[b.Key])
            .ToList();

        List<List<string>> values = new();
        foreach(KeyValuePair<int, List<WeightedItem>> kvp in optimalGodBuildsByDate.BuildsByQueue)
        {
            List<WeightedItem> build = kvp.Value;
            List<string> row = build.Select(i => CreateTableCell(i)).ToList();

            // Append the summary
            Item stats = build.Aggregate(new Item(), (total, item) => total + (item == null ? new Item() : ItemsById[item.ItemId]));
            row.Add(CreateTableCell(stats, GodsById[optimalGodBuildsByDate.GodId]));

            // Add row to table values.
            values.Add(row);
        }

        Table godBuilds = new()
        {
            HeaderRow = headerRow,
            HeaderColumn = headerColumn,
            Values = values
        };

        return godBuilds;
    }

    public static string CreateTableCell(WeightedItem item)
    {
        if(item is null)
        {
            return $"<div class=\"d-flex flex-column flex-shrink-0\"><div class=\"flex-row\"><img class=\"item-thumbnail rounded border shadow border-dark\"></div><div class=\"flex-row\"></div><div class=\"flex-row\">-</div><div class=\"flex-row\">-</div></div>";
        }

        return $"<div class=\"d-flex flex-column flex-shrink-0\"><div class=\"flex-row tooltip-parent\"><img class=\"item-thumbnail rounded shadow border border-dark\" src=\"{ItemsById[item.ItemId].ItemIconUrl}\" data-toggle=\"tooltip\" data-html=\"true\" rel=\"tooltip\" title=\"{ItemsById[item.ItemId].ToTooltip()}\"></div><div class=\"flex-row\">Pick: {item.Popularity / (float)item.TotalMatches * 100f:.00} %</div><div class=\"flex-row\">Win: {item.WinRate:.00} %</div></div>";
    }

    public static string CreateTableCell(Item item, God god)
    {
        if (item is null)
        {
            return $"<div class=\"d-flex flex-column flex-shrink-0\"><div class=\"flex-row\"><img class=\"item-thumbnail rounded border shadow border-dark\"></div></div>";
        }

        return $"<div class=\"d-flex flex-column flex-shrink-0\"><div class=\"flex-row tooltip-parent\"><img class=\"item-thumbnail rounded shadow border border-dark\" src=\"{god.GodIconUrl}\" data-toggle=\"tooltip\" data-html=\"true\" rel=\"tooltip\" title=\"{item.ToBuildSummary(god, 20)}\"></div></div>";
    }

    public static void ExportOptimalGodBuildsAsPage(int godId, string patch, bool update = false)
    {
        GodBuildData optimalGodBuildsByDate = null;

        MatchPlayerDao matchPlayerDao = new(Config.Database);
        MatchDateRange = matchPlayerDao.GetDateRange(Patches[patch]);

        if (update)
        {
            optimalGodBuildsByDate = GetOptimalGodBuildByPatch(godId, patch);
            JsonController.ExportOptimalBuild(godId, patch, optimalGodBuildsByDate);
        }
        else
        {
            optimalGodBuildsByDate = JsonController.ImportOptimalBuild(godId, patch);
        }

        Table table = GetOptimalGodBuildTable(optimalGodBuildsByDate);

        OptimalGodBuilds page = new(godId, patch, table.ToString());
        page.Export();
    }
}
