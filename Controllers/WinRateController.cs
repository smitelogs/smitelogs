﻿using SmiteLogs.Dao;
using SmiteLogs.Models;
using SmiteLogs.Models.Matches;
using SmiteLogs.Models.Webpage;

namespace SmiteLogs.Controllers;

public class WinRateController
{
    public static void ExportGodWinRatesByQueueAsPage(string patch, bool update = false)
    {
        List<GodMatchData> godWinRatesByQueue = null;

        MatchPlayerDao matchPlayerDao = new(Config.Database);
        MatchDateRange = matchPlayerDao.GetDateRange(Patches[patch]);

        if (update)
        {
            godWinRatesByQueue = GetAllGodWinsRatesByPatch(patch);
            JsonController.ExportGodWinRates(patch, godWinRatesByQueue);
        }
        else
        {
            godWinRatesByQueue = JsonController.ImportGodWinRates(patch);
        }

        Table table = GetGodQueueWinRatesAsTable(godWinRatesByQueue, patch);

        GodWinRates page = new(table.ToString(), patch);
        page.Export();
    }

    public static Table GetGodQueueWinRatesAsTable(List<GodMatchData> godWinRatesByPatch, string patch)
    {
        List<string> headerRow = QueuesById.Values.ToList();
        headerRow.Insert(0, "God");

        List<string> headerColumn = godWinRatesByPatch
            .Select(g => 
                $"<a class=\"text-white\" href=\"./builds/optimal/{patch}/{GodsById[g.GodId].Name.ToLower().Replace(' ', '-').Replace('\'', '-')}/index.html\"><div class=\"flex-column\"><div class=\"flex-row\"><img class=\"god-thumbnail rounded shadow border border-dark\" src=\"{GodsById[g.GodId].GodIconUrl}\"></div><div class=\"flex-row\">{ GodsById[g.GodId].Name}</div></div></a>"
            )
            .ToList();

        List<List<string>> values = godWinRatesByPatch
            .Select(gods => gods.WinRatesByQueue
                .Select(g => $"{g.Value} %")
                .ToList())
            .ToList();

        Table godWinRates = new()
        {
            HeaderRow = headerRow,
            HeaderColumn = headerColumn,
            Values = values
        };

        return godWinRates;
    }

    public static List<GodMatchData> GetAllGodWinsRatesByPatch(string patch = null)
    {
        patch ??= Patches.Keys.First();

        List<GodMatchData> godWinRatesByDate = new();

        GodDao godDao = new(Config.Database);
        List<Task> taskList = new();

        foreach (int godId in GodsById.Keys)
        {
            taskList.Add(Task.Run(() => GetGodWinRatesByDate(godId, patch)));
        }

        Task t = Task.WhenAll(taskList.ToArray());
        t.Wait();

        foreach (Task<GodMatchData> task in taskList)
        {
            godWinRatesByDate.Add(task.Result);
        }

        return godWinRatesByDate;
    }

    public static GodMatchData GetGodWinRatesByDate(int godId, string patch = null)
    {
        patch ??= Patches.Keys.First();
        MatchPlayerDao matchPlayerDao = new(Config.Database);

        GodMatchData godMatchData = new()
        {
            GodId = godId,
            DateRange = Patches[patch],
            WinRatesByQueue = new(),
        };

        foreach (int queue in QueuesById.Keys)
        {
            float winRate = matchPlayerDao.GetWinRate(godId, queue, patch);
            godMatchData.WinRatesByQueue.Add(queue, winRate);
        }

        return godMatchData;
    }
}
